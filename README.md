
 **ALittleRPG**
--

Still in development.
Code is currently working on MacOSX 10.14.1

#### Dependencies
[Universal LPC Spritesheet]: https://github.com/jrconway3/Universal-LPC-spritesheet
[pugixml]: https://pugixml.org/

- Native C++ libs
- SDL2
- SDL2_image
- SDL2_ttf
- [Universal LPC Spritesheet] 
- [pugixml]

Actual version render:
![Alt Text](https://media.giphy.com/media/9RZyNLam31vWiU5YGI/giphy.gif)