//
// Created by Théophile Vieux on 2019-01-19.
//

#ifndef ALITTLERPG_TIMER_H
#define ALITTLERPG_TIMER_H

#include <chrono>

class Timer {
public:
    Timer();

    void init();

    double getTime();

    float getElapsedTime();

    double getLastLoopTime();

private:
    double lastLoopTime;


};


#endif //ALITTLERPG_TIMER_H
