//
// Created by Théophile Vieux on 2019-01-16.
//

#ifndef ALITTLERPG_GAMEOBJECT_H
#define ALITTLERPG_GAMEOBJECT_H

#include <string>
#include "../../graphic/Rendered.h"

class GameElement : public Rendered {
public:
    explicit GameElement(int id);

    void init(Window *window) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void draw(Window *window, Renderer *renderer) override;

    void cleanup() override;

    char * getName();

    void setName(char *name);

private:
    char* name;

};


#endif //ALITTLERPG_GAMEOBJECT_H
