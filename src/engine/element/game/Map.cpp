//
// Created by Théophile Vieux on 2019-02-04.
//

#include "Map.h"

Map::Map(int id, char *mapName) : GameElement(id), mapName(mapName) {

}

void Map::load() {
    auto map = TmxParser::loadTilemapFromTMXFile(this->mapName);
}

