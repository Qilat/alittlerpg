

//
// Created by Théophile Vieux on 2019-01-16.
//

#include "GameElement.h"

GameElement::GameElement(int id) : name(const_cast<char *>("No Name")) {
    this->id = id;
}

void GameElement::init(Window *window) {

}

void GameElement::input(Window *window, Inputs *inputs) {

}

void GameElement::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {

}

void GameElement::draw(Window *window, Renderer *renderer) {

}

void GameElement::cleanup() {

}

char *GameElement::getName() {
    return name;
}

void GameElement::setName(char *name) {
    GameElement::name = name;
}
