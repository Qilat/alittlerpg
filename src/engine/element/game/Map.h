//
// Created by Théophile Vieux on 2019-02-04.
//

#ifndef ALITTLERPG_MAP_H
#define ALITTLERPG_MAP_H

#include "GameElement.h"
#include "../../tmx/parser/TmxParser.h"


class Map : public GameElement {

public:
    Map(int id, char *mapName);

    void load();


private:
    char *mapName = nullptr;
    //Tilemap* map = nullptr;
};


#endif //ALITTLERPG_MAP_H
