//
// Created by Théophile Vieux on 2019-01-20.
//

#ifndef ALITTLERPG_COLOREDBUTTON_H
#define ALITTLERPG_COLOREDBUTTON_H

#include "Button.h"

class ColoredButton : public Button {
public:
    ColoredButton(int id, char *text);

    ColoredButton(int id, char *text, SDL_Color *color);

protected:
    void drawHover(Renderer *renderer) override;

    void drawNormal(Renderer *renderer) override;

    void drawClicked(Renderer *renderer) override;

public:

    SDL_Color *getColor() const;

    void setColor(SDL_Color *color);

    SDL_Color *color;
};


#endif //ALITTLERPG_COLOREDBUTTON_H
