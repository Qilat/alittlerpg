//
// Created by Théophile Vieux on 2019-01-20.
//

#include "Button.h"

Button::Button(int id) : UIElement(id), onPressListener(nullptr), onReleaseListener(nullptr), text(nullptr) {

}

Button::Button(int id, char *text) : UIElement(id), text(text), onPressListener(nullptr), onReleaseListener(nullptr) {}


void Button::draw(Window *window, Renderer *renderer) {
    if (this->clicked) {
        this->drawClicked(renderer);
    } else if (this->hover) {
        this->drawHover(renderer);
    } else {
        this->drawNormal(renderer);
    }
}

void Button::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    SDL_Point mousePos = {inputs->mouseX, inputs->mouseY};
    SDL_Rect buttonBox = {this->x, this->y, this->width, this->height};
    this->hover = SDL_PointInRect(&mousePos, &buttonBox) == SDL_TRUE;
}

void Button::input(Window *window, Inputs *inputs) {

    bool leftClickPress = inputs->mouse[SDL_BUTTON_LEFT];

    if (this->hover) {
        if (!this->clicked && leftClickPress) {
            this->clicked = true;
            if (this->onPressListener != nullptr)
                this->onPressListener(this);
            return;
        }

        if (this->clicked && !leftClickPress) {
            this->clicked = false;
            if (this->onReleaseListener != nullptr)
                this->onReleaseListener(this);
            return;
        }
    } else {
        if (this->clicked) {
            this->clicked = false;
            return;
        }
    }
}

void Button::setOnReleaseListener(void (*onReleaseListener)(Button *)) {
    Button::onReleaseListener = onReleaseListener;
}

void Button::setOnPressListener(void (*onPressListener)(Button *)) {
    Button::onPressListener = onPressListener;
}

void Button::drawHover(Renderer *renderer) {

}

void Button::drawNormal(Renderer *renderer) {

}

void Button::drawClicked(Renderer *renderer) {

}







