//
// Created by Théophile Vieux on 2019-01-20.
//

#include "ColoredButton.h"


ColoredButton::ColoredButton(int id, char *text, SDL_Color *color) : Button(id, text), color(color) {}

ColoredButton::ColoredButton(int id, char *text) : ColoredButton(id, text, nullptr) {}

SDL_Color *ColoredButton::getColor() const {
    return color;
}

void ColoredButton::setColor(SDL_Color *color) {
    ColoredButton::color = color;
}

void ColoredButton::drawHover(Renderer *renderer) {
    SDL_Color *newColor = renderer->getDeltaedColor(this->color, 10);
    renderer->drawColoredRect(newColor, this->x, this->y, this->width, this->height);
    renderer->drawString(this->text, this->x, this->y, this->width, this->height);
}

void ColoredButton::drawNormal(Renderer *renderer) {
    renderer->drawColoredRect(this->color, this->x, this->y, this->width, this->height);
    renderer->drawString(this->text, this->x, this->y, this->width, this->height);
}

void ColoredButton::drawClicked(Renderer *renderer) {
    SDL_Color *newColor = renderer->getDeltaedColor(this->color, 20);
    renderer->drawColoredRect(newColor, this->x, this->y, this->width, this->height);
    renderer->drawString(this->text, this->x, this->y, this->width, this->height);
}





