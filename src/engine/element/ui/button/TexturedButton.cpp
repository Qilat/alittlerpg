//
// Created by Théophile Vieux on 2019-01-20.
//

#include "TexturedButton.h"

TexturedButton::TexturedButton(int id) : TexturedButton(id, nullptr, nullptr, nullptr, nullptr) {}

TexturedButton::TexturedButton(int id, char *textureName) : TexturedButton(id, nullptr, textureName, nullptr, nullptr) {
}

TexturedButton::TexturedButton(int id, char *text, char *textureName) : TexturedButton(id, text, textureName, nullptr,
                                                                                       nullptr) {}

TexturedButton::TexturedButton(int id, char *text, char *normalTexturePath, char *hoverTexturePath,
                               char *clickedTexturePath) : Button(id, text), normalTexturePath(normalTexturePath),
                                                           hoverTexturePath(hoverTexturePath),
                                                           clickedTexturePath(clickedTexturePath) {}

void TexturedButton::drawHover(Renderer *renderer) {
    renderer->drawTexture(this->hoverTexturePath, this->x, this->y, this->width, this->height);
    renderer->drawString(this->text, this->x, this->y, this->width, this->height);

}

void TexturedButton::drawNormal(Renderer *renderer) {
    renderer->drawTexture(this->normalTexturePath, this->x, this->y, this->width, this->height);
    renderer->drawString(this->text, this->x, this->y, this->width, this->height);
}

void TexturedButton::drawClicked(Renderer *renderer) {
    renderer->drawTexture(this->clickedTexturePath, this->x, this->y, this->width, this->height);
    renderer->drawString(this->text, this->x, this->y, this->width, this->height);
}

char *TexturedButton::getNormalTexturePath() const {
    return normalTexturePath;
}

void TexturedButton::setNormalTexturePath(char *normalTexturePath) {
    TexturedButton::normalTexturePath = normalTexturePath;
}

char *TexturedButton::getHoverTexturePath() const {
    return hoverTexturePath;
}

void TexturedButton::setHoverTexturePath(char *hoverTexturePath) {
    TexturedButton::hoverTexturePath = hoverTexturePath;
}

char *TexturedButton::getClickedTexturePath() const {
    return clickedTexturePath;
}

void TexturedButton::setClickedTexturePath(char *clickedTexturePath) {
    TexturedButton::clickedTexturePath = clickedTexturePath;
}






