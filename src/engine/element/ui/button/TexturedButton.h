//
// Created by Théophile Vieux on 2019-01-20.
//

#ifndef ALITTLERPG_TEXTUREDBUTTON_H
#define ALITTLERPG_TEXTUREDBUTTON_H

#include <string>
#include "Button.h"

class TexturedButton : public Button {
public:
    explicit TexturedButton(int id);

    TexturedButton(int id, char *textureName);

    TexturedButton(int id, char *text, char *textureName);

    TexturedButton(int id, char *text, char *normalTexturePath, char *hoverTexturePath, char *clickedTexturePath);

    char *getNormalTexturePath() const;

    void setNormalTexturePath(char *normalTexturePath);

    char *getHoverTexturePath() const;

    void setHoverTexturePath(char *hoverTexturePath);

    char *getClickedTexturePath() const;

    void setClickedTexturePath(char *clickedTexturePath);

protected:
    void drawHover(Renderer *renderer) override;

    void drawNormal(Renderer *renderer) override;

    void drawClicked(Renderer *renderer) override;

private:
    char *normalTexturePath;
    char *hoverTexturePath;
    char *clickedTexturePath;
};


#endif //ALITTLERPG_TEXTUREDBUTTON_H
