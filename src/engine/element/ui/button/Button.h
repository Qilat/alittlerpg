//
// Created by Théophile Vieux on 2019-01-20.
//

#ifndef ALITTLERPG_BUTTON_H
#define ALITTLERPG_BUTTON_H

#include "../UIElement.h"

class Button : public UIElement {

protected:
    explicit Button(int id);

    Button(int id, char *text);

    virtual void drawHover(Renderer *renderer);

    virtual void drawNormal(Renderer *renderer);

    virtual void drawClicked(Renderer *renderer);

public:
    void draw(Window *window, Renderer *renderer) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void setOnPressListener(void (*onPressListener)(Button *));

    void setOnReleaseListener(void (*onReleaseListener)(Button *));

protected:
    char *text;
    bool hover = false;
    bool clicked = false;

    void (*onPressListener)(Button *);

    void (*onReleaseListener)(Button *);
};


#endif //ALITTLERPG_BUTTON_H
