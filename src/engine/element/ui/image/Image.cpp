//
// Created by Théophile Vieux on 2019-01-21.
//

#include "Image.h"

Image::Image(int id) : UIElement(id), path(nullptr) {}

Image::Image(int id, char *path) : UIElement(id), path(path) {}

char *Image::getPath() const {
    return path;
}

void Image::setPath(char *path) {
    Image::path = path;
}

void Image::draw(Window *window, Renderer *renderer) {
    renderer->drawTexture(this->path, this->x, this->y, this->width, this->height);
}

void Image::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {

}


