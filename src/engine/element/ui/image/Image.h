//
// Created by Théophile Vieux on 2019-01-21.
//

#ifndef ALITTLERPG_IMAGE_H
#define ALITTLERPG_IMAGE_H

#include "../UIElement.h"

class Image : public UIElement {

public:
    explicit Image(int id);

    Image(int id, char *path);

    char *getPath() const;

    void setPath(char *path);

    void draw(Window *window, Renderer *renderer) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

private:
    char *path;

};


#endif //ALITTLERPG_IMAGE_H
