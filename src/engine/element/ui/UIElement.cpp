//
// Created by Théophile Vieux on 2019-01-20.
//

#include "UIElement.h"

UIElement::UIElement(int id) {
    this->id = id;
}

void UIElement::init(Window *window) {

}

void UIElement::draw(Window *window, Renderer *renderer) {

}

void UIElement::input(Window *window, Inputs *inputs) {

}

void UIElement::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {

}

void UIElement::cleanup() {

}




