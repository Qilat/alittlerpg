//
// Created by Théophile Vieux on 2019-01-20.
//

#ifndef ALITTLERPG_UIELEMENT_H
#define ALITTLERPG_UIELEMENT_H

#include "../../graphic/Rendered.h"

class UIElement : public Rendered {
public:
    explicit UIElement(int id);

    void init(Window *window) override;

    void draw(Window *window, Renderer *renderer) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void cleanup() override;
};


#endif //ALITTLERPG_UIELEMENT_H
