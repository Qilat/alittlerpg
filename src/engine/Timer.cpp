//
// Created by Théophile Vieux on 2019-01-19.
//

#include "Timer.h"

void Timer::init() {
    this->lastLoopTime = getTime();
}

double Timer::getTime() {
    return std::chrono::time_point_cast<std::chrono::nanoseconds>(
            std::chrono::high_resolution_clock::now()).time_since_epoch().count() / 1000000000.0;
}

float Timer::getElapsedTime() {
    double time = getTime();
    auto elapsedTime = (float) (time - lastLoopTime);
    this->lastLoopTime = time;
    return elapsedTime;
}

double Timer::getLastLoopTime() {
    return this->lastLoopTime;
}

Timer::Timer() = default;
