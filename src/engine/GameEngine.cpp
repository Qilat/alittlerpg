
//
// Created by Théophile Vieux on 2019-01-15.
//

#define TARGET_FPS 60.0f
#define TARGET_UPS 40.0f

#include "GameEngine.h"

void GameEngine::run() {
    this->init();
    this->loop();
    this->destroy();
}

GameEngine::GameEngine(string windowTitle, int width, int height) {
    window = new Window(std::move(windowTitle), width, height);
    this->inputs = new Inputs();
    this->timer = new Timer();
}


void GameEngine::init() {
    this->window->init();
    this->renderer = new Renderer(this->window->getSdlRenderer());
    this->timer->init();
}

void GameEngine::destroy() {
    this->window->destroy();
    this->currentStage->cleanup();
}

void GameEngine::input() {
    this->getInputs()->updateInputs();
    this->currentStage->input(this->window, this->getInputs());
}

void GameEngine::loop() {
    float accumulator = 0.0f;
    float interval = 1.0f / TARGET_UPS;

    while (!this->isFinish()) {
        float elapsedTime = this->timer->getElapsedTime();
        accumulator += elapsedTime;

        this->input();

        while (accumulator >= interval) {
            this->update(interval);
            accumulator -= interval;
        }
        render(interval);
        this->sync();
    }
}

void GameEngine::sync() {
    float loopSlot = 1.0f / TARGET_FPS;
    double endTime = this->timer->getLastLoopTime() + loopSlot;
    while (this->timer->getTime() < endTime) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}


void GameEngine::update(float delta) {
    if (this->inputs->quit == SDL_TRUE) {
        this->setFinish(true);
    }
    this->window->updateDimAndPositions();
    this->currentStage->update(this->window, delta, this->inputs, this->renderer);
}

void GameEngine::render(float delta) {
    //CLEAN SCREEN
    Utils::catchSDLError(SDL_SetRenderDrawColor(this->window->getSdlRenderer(), 255, 255, 255, 255));
    Utils::catchSDLError(SDL_RenderClear(this->window->getSdlRenderer()));

    //DRAW CURRENT STAGE
    this->currentStage->draw(this->window, this->renderer);

    //DRAW FPS ON TOP OF THE SCREEN
    double fps = 1 / (this->timer->getTime() - this->timer->getLastLoopTime());
    const string strFps = std::to_string(fps);
    this->renderer->drawString(const_cast<char *>(strFps.c_str()), 0, 0, 300, 50);

    //RENDER
    SDL_RenderPresent(this->window->getSdlRenderer());
}

Stage *GameEngine::getCurrentStage() const {
    return currentStage;
}

void GameEngine::setCurrentStage(Stage *currentStage) {
    this->currentStage = currentStage;
    this->currentStage->init(this->window);
}

Inputs *GameEngine::getInputs() const {
    return inputs;
}

bool GameEngine::isFinish() const {
    return finish;
}

void GameEngine::setFinish(bool finish) {
    GameEngine::finish = finish;
}

Window *GameEngine::getWindow() const {
    return window;
}

SDL_Renderer *GameEngine::getSdlRenderer() const {
    return this->window->getSdlRenderer();
}

Renderer *GameEngine::getRenderer() const {
    return this->renderer;
}

