//
// Created by Théophile Vieux on 2019-01-16.
//

#ifndef ALITTLERPG_UTILS_H
#define ALITTLERPG_UTILS_H

#include <iostream>
#include "SDLBinder.h"

class Utils {
public:
    static void catchSDLError(int code);
};


#endif //ALITTLERPG_UTILS_H
