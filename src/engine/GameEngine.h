//
// Created by Théophile Vieux on 2019-01-15.
//

#ifndef ALITTLERPG_GAME_H
#define ALITTLERPG_GAME_H

#include <utility>
#include <string>
#include <thread>
#include "stage/Stage.h"
#include "Timer.h"

class GameEngine {
public:
    GameEngine(string windowTitle, int width, int height);

    void init();

    void run();

    void update(float delta);

    void render(float delta);

    void input();

    void loop();

    void sync();

    void destroy();

    bool isFinish() const;

    void setFinish(bool finish);

    Inputs *getInputs() const;

    Stage *getCurrentStage() const;

    void setCurrentStage(Stage *currentStage);

    Window *getWindow() const;
    SDL_Renderer* getSdlRenderer() const;

    Renderer *getRenderer() const;
private:
    Window *window = nullptr;
    Timer *timer = nullptr;
    Inputs *inputs = nullptr;
    Stage *currentStage = nullptr;
    Renderer *renderer = nullptr;

    bool finish;
};

#endif //ALITTLERPG_GAME_H
