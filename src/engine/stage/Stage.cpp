//
// Created by Théophile Vieux on 2019-01-16.
//

#include "Stage.h"

void Stage::init(Window *window) {
    if (this->userInterface != nullptr)
        this->userInterface->init(window);
}

void Stage::input(Window *window, Inputs *inputs) {
    if (this->userInterface != nullptr)
        this->userInterface->input(window, inputs);
}

void Stage::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    if (this->userInterface != nullptr)
        this->userInterface->update(window, interval, inputs, renderer);
}

void Stage::draw(Window *window, Renderer *renderer) {
    if (this->userInterface != nullptr)
        this->userInterface->draw(window, renderer);
}


Ui *Stage::getUserInterface() {
    return userInterface;
}

void Stage::setUserInterface(Ui *userInterface) {
    Stage::userInterface = userInterface;
}





