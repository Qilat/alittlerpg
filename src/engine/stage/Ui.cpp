//
// Created by Théophile Vieux on 2019-01-22.
//

#include "Ui.h"


void Ui::init(Window *window) {
}

void Ui::input(Window *window, Inputs *inputs) {
    for (UIElement *element : this->uiElements) {
        element->input(window, inputs);
    }
}

void Ui::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    for (UIElement *element : this->uiElements) {
        element->update(window, interval, inputs, renderer);
    }
}

void Ui::draw(Window *window, Renderer *renderer) {
    for (UIElement *element : this->uiElements) {
        element->draw(window, renderer);
    }
}

void Ui::addElement(UIElement *element) {
    this->uiElements.push_back(element);
}

void Ui::cleanup() {

}
