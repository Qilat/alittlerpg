//
// Created by Théophile Vieux on 2019-01-16.
//

#include "MenuStage.h"

MenuStage::MenuStage() = default;

void MenuStage::init(Window *window) {
    Stage::init(window);
}

void MenuStage::draw(Window *window, Renderer *renderer) {
    Stage::draw(window, renderer);
}

void MenuStage::input(Window *window, Inputs *inputs) {
    Stage::input(window, inputs);
}

void MenuStage::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    Stage::update(window, interval, inputs, renderer);
}

void MenuStage::cleanup() {
}



