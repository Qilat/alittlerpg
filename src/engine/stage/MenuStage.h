//
// Created by Théophile Vieux on 2019-01-16.
//

#ifndef ALITTLERPG_MENUSTAGE_H
#define ALITTLERPG_MENUSTAGE_H

#include "Stage.h"

class MenuStage : public Stage {
public:
    MenuStage();

    void init(Window *window) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void draw(Window *window, Renderer *renderer) override;

    void cleanup() override;
};


#endif //ALITTLERPG_MENUSTAGE_H
