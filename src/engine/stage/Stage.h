//
// Created by Théophile Vieux on 2019-01-16.
//

#ifndef ALITTLERPG_STAGE_H
#define ALITTLERPG_STAGE_H

#include "Ui.h"

class Stage : public Rendered {
public:
    void init(Window *window) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void draw(Window *window, Renderer *renderer) override;

    void cleanup() override = 0;

    Ui *getUserInterface();

    void setUserInterface(Ui *userInterface);

private:
    Ui *userInterface = nullptr;
};


#endif //ALITTLERPG_STAGE_H
