//
// Created by Théophile Vieux on 2019-01-22.
//

#ifndef ALITTLERPG_UI_H
#define ALITTLERPG_UI_H

#include <vector>
#include "../element/ui/UIElement.h"

class Ui : public Rendered {
public:
    void init(Window *window) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void draw(Window *window, Renderer *renderer) override;

    void cleanup() override;

    virtual void addElement(UIElement *element);

protected:
    vector<UIElement *> uiElements;

};


#endif //ALITTLERPG_UI_H
