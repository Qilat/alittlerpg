//
// Created by Théophile Vieux on 2019-01-16.
//

#ifndef ALITTLERPG_INPUTS_H
#define ALITTLERPG_INPUTS_H

#include "SDLBinder.h"

class Inputs {
public:

    void updateInputs();

    SDL_bool key[SDL_NUM_SCANCODES];
    SDL_bool quit;
    int mouseX, mouseY, deltaMouseX, deltaMouseY;
    int xwheel, ywheel;
    SDL_bool mouse[6];


};


#endif //ALITTLERPG_INPUTS_H
