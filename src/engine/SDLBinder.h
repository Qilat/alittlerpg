//
// Created by Théophile Vieux on 2019-01-31.
//

#ifndef ALITTLERPG_SDLBINDER_H
#define ALITTLERPG_SDLBINDER_H

//Import can be change for xcode build or cmake only here. Example : For Xcode = SDL2/SDL.h and for cmake only SDL.h

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#endif //ALITTLERPG_SDLBINDER_H
