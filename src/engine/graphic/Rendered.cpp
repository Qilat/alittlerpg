//
// Created by Théophile Vieux on 2019-01-27.
//

#include "Rendered.h"

int Rendered::getId() const {
    return id;
}

int Rendered::getX() const {
    return x;
}

void Rendered::setX(int x) {
    Rendered::x = x;
}

int Rendered::getY() const {
    return y;
}

void Rendered::setY(int y) {
    Rendered::y = y;
}

int Rendered::getWidth() const {
    return width;
}

void Rendered::setWidth(int width) {
    Rendered::width = width;
}

int Rendered::getHeight() const {
    return height;
}

void Rendered::setHeight(int height) {
    Rendered::height = height;
}
