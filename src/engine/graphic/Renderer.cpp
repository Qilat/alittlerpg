//
// Created by Théophile Vieux on 2019-01-16.
//

#include "Renderer.h"


Renderer::Renderer(SDL_Renderer *sdlRenderer) {
    this->sdlRenderer = sdlRenderer;
}

void Renderer::drawBackgroundColor(SDL_Renderer *renderer, SDL_Color color) {
    Utils::catchSDLError(SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a));
    Utils::catchSDLError(SDL_RenderClear(renderer));
}

SDL_Texture *Renderer::loadTexture(SDL_Renderer* renderer, const char *path) {
    return IMG_LoadTexture(renderer, path);
}

SDL_Texture *Renderer::loadTexture(const char *path) {
    return IMG_LoadTexture(this->sdlRenderer, path);
}

void Renderer::setTextureRenderTarget(SDL_Texture *texture) {
    SDL_SetRenderTarget(this->sdlRenderer, texture);
}

void Renderer::resetTextureRenderTarget() {
    SDL_SetRenderTarget(this->sdlRenderer, nullptr);
}

void Renderer::unloadTexture(SDL_Texture *texture) {
    SDL_DestroyTexture(texture);
}



void Renderer::drawTexture(SDL_Renderer *renderer, SDL_Texture *texture, int textureX, int textureY, int textureWidth,
                           int textureHeight, int x, int y, int width, int height) {
    SDL_Rect texturePosition = {textureX, textureY, textureWidth, textureHeight};
    SDL_Rect position = {x, y, width, height};
    SDL_RenderCopy(renderer, texture, &texturePosition, &position);
}
void Renderer::drawTexture(SDL_Texture *texture, int textureX, int textureY, int textureWidth, int textureHeight, int x,
                           int y, int width, int height) {
    Renderer::drawTexture(this->sdlRenderer, texture, textureX, textureY, textureWidth, textureHeight, x, y, width,
                          height);
}

void Renderer::drawTexture(SDL_Renderer *renderer, const char *path, int textureX, int textureY, int textureWidth,
                           int textureHeight, int x, int y, int width, int height) {
    SDL_Texture *texture = Renderer::loadTexture(renderer, path);
    Renderer::drawTexture(renderer, texture, textureX, textureY, textureWidth, textureHeight, x, y, width, height);
    Renderer::unloadTexture(texture);
}
void Renderer::drawTexture(const char *path, int textureX, int textureY, int textureWidth, int textureHeight, int x, int y,
                      int width, int height) {
    Renderer::drawTexture(this->sdlRenderer, path, textureX, textureY, textureWidth, textureHeight, x, y, width,
                          height);
}

void Renderer::drawTexture(SDL_Renderer *renderer, SDL_Texture *texture, int x, int y, int width, int height) {
    SDL_Rect position = {x, y, width, height};
    SDL_RenderCopy(renderer, texture, nullptr, &position);
}
void Renderer::drawTexture(SDL_Texture *texture, int x, int y, int width, int height) {
    Renderer::drawTexture(this->sdlRenderer, texture, x, y, width, height);
}

void Renderer::drawTexture(SDL_Renderer *renderer, const char *path, int x, int y, int width, int height) {
    SDL_Texture *texture = loadTexture(renderer, path);
    Renderer::drawTexture(renderer, texture, x, y, width, height);
    Renderer::unloadTexture(texture);
}
void Renderer::drawTexture(const char *path, int x, int y, int width, int height) {
    Renderer::drawTexture(this->sdlRenderer, path, x, y, width, height);
}


void Renderer::drawColoredRect(SDL_Color *color, int x, int y, int width, int height) {
    SDL_SetRenderDrawColor(this->sdlRenderer, color->r, color->g, color->b, color->a);
    SDL_Rect rect = {x, y, width, height};
    SDL_RenderFillRect(this->sdlRenderer, &rect);
}

void Renderer::drawString(char *string, int x, int y, int width, int height) {
    SDL_Color color = {0, 0, 0, 255};
    Renderer::drawString(string, color, x, y, width, height);
}

void Renderer::drawString(char *string, SDL_Color color, int x, int y, int width, int height) {
    TTF_Font *font = TTF_OpenFont("font/arial.ttf", 60);

    SDL_Surface *text = TTF_RenderText_Blended(font, string, color);
    TTF_CloseFont(font);

    SDL_Texture *texture = SDL_CreateTextureFromSurface(this->sdlRenderer, text);

    Renderer::drawTexture(this->sdlRenderer, texture, x, y, width, height);
}

SDL_Color *Renderer::getDeltaedColor(SDL_Color *color, Uint8 delta) {
    Uint8 min = 0;
    Uint8 newR = (delta >= color->r) ? min : (color->r - delta);
    Uint8 newG = (delta >= color->g) ? min : (color->g - delta);
    Uint8 newB = (delta >= color->b) ? min : (color->b - delta);
    return new SDL_Color{newR, newG, newB, color->a};
}

void Renderer::getTextureDim(SDL_Texture* texture, int *width, int *height) {
    Utils::catchSDLError(SDL_QueryTexture(texture, nullptr, nullptr, width, height));
}
