
//
// Created by Théophile Vieux on 2019-01-15.
//

#include <utility>
#include "Window.h"

Window::Window() {
}

Window::Window(string name, int width, int height) : windowName(std::move(name)), width(width),
                                                     height(height) {
}

void Window::init() {
    this->sdlWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                       width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (sdlWindow == nullptr) {
        fprintf(stderr, "Erreur lors de la création d'une fenêtre : %s\n", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    this->sdlRenderer = SDL_CreateRenderer(this->sdlWindow, -1, SDL_RENDERER_ACCELERATED);
    if (this->sdlRenderer == nullptr) {
        this->sdlRenderer = SDL_CreateRenderer(this->sdlWindow, -1, SDL_RENDERER_SOFTWARE);
        if (this->sdlRenderer == nullptr) {
            fprintf(stderr, "Erreur lors de la création du graphic : %s\n", SDL_GetError());
            SDL_Quit();
            exit(EXIT_FAILURE);
        }
    }

}

void Window::destroy() {
    SDL_DestroyRenderer(this->sdlRenderer);
    SDL_DestroyWindow(this->sdlWindow);
}

SDL_Window *Window::getSdlWindow() const {
    return sdlWindow;
}

SDL_Renderer *Window::getSdlRenderer() const {
    return sdlRenderer;
}

void Window::updateDimAndPositions() {
    SDL_GetWindowSize(this->getSdlWindow(), &this->width, &this->height);
}

const string &Window::getWindowName() const {
    return windowName;
}

int Window::getX() const {
    return x;
}

int Window::getY() const {
    return y;
}

int Window::getWidth() const {
    return width;
}

int Window::getHeight() const {
    return height;
}
