//
// Created by Théophile Vieux on 2019-01-15.
//

#ifndef ALITTLERPG_WINDOW_H
#define ALITTLERPG_WINDOW_H

#include <string>
#include <iostream>
#include "../SDLBinder.h"

using namespace std;

class Window {
public:
    Window();

    Window(string name, int width, int height);

    void init();

    void updateDimAndPositions();

    void destroy();

    SDL_Window *getSdlWindow() const;

    SDL_Renderer *getSdlRenderer() const;

    const string &getWindowName() const;

    int getX() const;

    int getY() const;

    int getWidth() const;

    int getHeight() const;

private:
    SDL_Window *sdlWindow = nullptr;
    string windowName = "No name";
    int x = 0;
    int y = 0;
    int width = 100;
    int height = 100;
    SDL_Renderer *sdlRenderer = nullptr;
};


#endif //ALITTLERPG_WINDOW_H
