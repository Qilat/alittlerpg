//
// Created by Théophile Vieux on 2019-01-27.
//

#ifndef ALITTLERPG_RENDERED_H
#define ALITTLERPG_RENDERED_H

#include "Window.h"
#include "../Inputs.h"
#include "Renderer.h"

class Rendered {
public:
    virtual void init(Window *window) = 0;

    virtual void input(Window *window, Inputs *inputs) = 0;

    virtual void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) = 0;

    virtual void draw(Window *window, Renderer *renderer) = 0;

    virtual void cleanup() = 0;

    int getId() const;

    int getX() const;

    virtual void setX(int x);

    int getY() const;

    virtual void setY(int y);

    int getWidth() const;

    virtual void setWidth(int width);

    int getHeight() const;

    virtual void setHeight(int height);

protected:
    int id = 0;
    int x = 0;
    int y = 0;
    int width = 0;
    int height = 0;
};


#endif //ALITTLERPG_RENDERED_H
