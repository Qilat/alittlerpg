//
// Created by Théophile Vieux on 2019-01-16.
//

#ifndef ALITTLERPG_RENDERER_H
#define ALITTLERPG_RENDERER_H

#include "../Utils.h"

class Renderer {
public:

    explicit Renderer(SDL_Renderer *sdlRenderer);

    static SDL_Texture *loadTexture(SDL_Renderer *renderer, const char *path);

    SDL_Texture *loadTexture(const char path[]);

    void setTextureRenderTarget(SDL_Texture *texture);

    void resetTextureRenderTarget();

    static void unloadTexture(SDL_Texture *texture);

    static void drawTexture(SDL_Renderer *renderer, SDL_Texture *texture, int textureX, int textureY, int textureWidth,
                            int textureHeight, int x, int y, int width, int height);

    void drawTexture(SDL_Texture *texture, int textureX, int textureY, int textureWidth,
                     int textureHeight, int x, int y, int width, int height);

    static void drawTexture(SDL_Renderer *renderer, const char *path, int textureX, int textureY, int textureWidth,
                            int textureHeight, int x, int y, int width, int height);

    void drawTexture(const char *path, int textureX, int textureY, int textureWidth,
                     int textureHeight, int x, int y, int width, int height);

    static void drawTexture(SDL_Renderer *renderer, SDL_Texture *texture, int x, int y, int width, int height);

    void drawTexture(SDL_Texture *texture, int x, int y, int width, int height);

    static void drawTexture(SDL_Renderer *renderer, const char path[], int x, int y, int width, int height);

    void drawTexture(const char path[], int x, int y, int width, int height);

    void getTextureDim(SDL_Texture *texture, int *width, int *height);

    void drawColoredRect(SDL_Color *color, int x, int y, int width, int height);

    void drawString(char *string, int x, int y, int width, int height);

    void drawString(char *string, SDL_Color color, int x, int y, int width, int height);

    SDL_Color *getDeltaedColor(SDL_Color *color, Uint8 delta);

    static void drawBackgroundColor(SDL_Renderer *renderer, SDL_Color color);


private:
    SDL_Renderer *sdlRenderer;

};


#endif //ALITTLERPG_RENDERER_H
