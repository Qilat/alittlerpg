//
// Created by Théophile Vieux on 2019-01-16.
//

#include "Utils.h"


void Utils::catchSDLError(int code) {
    if (code != 0) {
        fprintf(stderr, "Error using SDL : %s", SDL_GetError());
        exit(EXIT_FAILURE);
    }
}