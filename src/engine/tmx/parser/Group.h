//
// Created by Théophile Vieux on 2019-02-03.
//

#ifndef ALITTLERPG_GROUP_H
#define ALITTLERPG_GROUP_H

#include <string>
#include <vector>
#include "layer/Layer.h"
#include "objectgroup/Objectgroup.h"
#include "ImageLayer.h"

class Group {

public:
    Group(int id, const string &name, int offsetx, int offsety, float opacity, int visible);

    void setProperties(Properties *properties);

    std::vector<Layer *> &getLayers();

    std::vector<Objectgroup *> &getObjectGroups();

    std::vector<ImageLayer *> &getImageLayers();

    std::vector<Group *> &getGroups();

private:
    int id;
    string name;
    int offsetx;
    int offsety;
    float opacity;
    int visible;

    Properties *properties;
    std::vector<Layer *> layers;
    std::vector<Objectgroup *> objectGroups;
    std::vector<ImageLayer *> imageLayers;
    std::vector<Group *> groups;

};


#endif //ALITTLERPG_GROUP_H
