//
// Created by Théophile Vieux on 2019-02-01.
//

#include "Layer.h"

Layer::Layer(int id, const std::string &name, int x, int y, int width, int height, float opacity, int visible,
             int offsetx, int offsety) : id(id), name(name), x(x), y(y), width(width), height(height), opacity(opacity),
                                         visible(visible), offsetx(offsetx), offsety(offsety) {}

void Layer::setProperties(Properties *properties) {
    Layer::properties = properties;
}

void Layer::setData(Data *data) {
    Layer::data = data;
}

Data *Layer::getData() const {
    return data;
}

const std::string &Layer::getName() const {
    return name;
}


