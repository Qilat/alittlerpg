//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_DATA_H
#define ALITTLERPG_DATA_H

#include <string>
#include "Chunk.h"

class Data {

public:
    Data(const std::string &encoding, const std::string &compression, const std::string &value);

    std::vector<LayerTile *> &getTiles();

    std::vector<Chunk *> &getChunks();

    const std::string &getValue() const;

private:
    std::string encoding;
    std::string compression;
    std::string value;

    std::vector<LayerTile *> tiles;
    std::vector<Chunk *> chunks;
};


#endif //ALITTLERPG_DATA_H
