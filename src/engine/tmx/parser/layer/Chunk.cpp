//
// Created by Théophile Vieux on 2019-02-01.
//

#include "Chunk.h"

Chunk::Chunk(int x, int y, int width, int height) : x(x), y(y), width(width), height(height) {}

std::vector<LayerTile *> &Chunk::getTiles() {
    return tiles;
}
