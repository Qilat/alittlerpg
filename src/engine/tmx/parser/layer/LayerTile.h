//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_LAYERTILE_H
#define ALITTLERPG_LAYERTILE_H


class LayerTile {
public:
    explicit LayerTile(int gid);

private:
    int gid;
};


#endif //ALITTLERPG_LAYERTILE_H
