//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_CHUNK_H
#define ALITTLERPG_CHUNK_H

#include <vector>
#include "LayerTile.h"

class Chunk {

public:
    Chunk(int x, int y, int width, int height);

    std::vector<LayerTile *> &getTiles();

private:
    int x;
    int y;
    int width;
    int height;

    std::vector<LayerTile *> tiles;

};


#endif //ALITTLERPG_CHUNK_H
