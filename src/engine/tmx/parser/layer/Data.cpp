//
// Created by Théophile Vieux on 2019-02-01.
//

#include "Data.h"

Data::Data(const std::string &encoding, const std::string &compression, const std::string &value) : encoding(encoding),
                                                                                                    compression(
                                                                                                            compression),
                                                                                                    value(value) {}

std::vector<LayerTile *> &Data::getTiles() {
    return this->tiles;
}

std::vector<Chunk *> &Data::getChunks() {
    return this->chunks;
}

const std::string &Data::getValue() const {
    return value;
}
