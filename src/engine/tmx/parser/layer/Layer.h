//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_LAYER_H
#define ALITTLERPG_LAYER_H

#include "../property/Properties.h"
#include "Data.h"

class Layer {

public:

    Layer(int id, const std::string &name, int x, int y, int width, int height, float opacity, int visible, int offsetx,
          int offsety);

    void setProperties(Properties *properties);

    void setData(Data *data);

    Data *getData() const;

    const std::string &getName() const;

private:
    int id;
    std::string name;
    int x;
    int y;
    int width;
    int height;
    float opacity;
    int visible;
    int offsetx;
    int offsety;

    Properties *properties;
    Data *data;
};


#endif //ALITTLERPG_LAYER_H
