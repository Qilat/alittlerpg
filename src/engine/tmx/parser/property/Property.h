//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_PROPERTY_H
#define ALITTLERPG_PROPERTY_H

#include <string>

class Property {
public:
    Property(const std::string &name, const std::string &type, const std::string &value);

private:
    std::string name;
    std::string type;
    std::string value;
};


#endif //ALITTLERPG_PROPERTY_H
