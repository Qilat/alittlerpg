//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_PROPERTIES_H
#define ALITTLERPG_PROPERTIES_H

#include <vector>
#include "Property.h"

class Properties {

public:
    std::vector<Property *> &getProperties();

private:
    std::vector<Property *> properties;
};


#endif //ALITTLERPG_PROPERTIES_H
