//
// Created by Théophile Vieux on 2019-02-01.
//

#include "Property.h"

Property::Property(const std::string &name, const std::string &type, const std::string &value) : name(name), type(type),
                                                                                                 value(value) {}
