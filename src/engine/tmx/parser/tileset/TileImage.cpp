
//
// Created by Théophile Vieux on 2019-02-01.
//

#include "TileImage.h"

TileImage::TileImage(string format, string source, string trans, int width, int height) : format(std::move(format)),
                                                                                  source(std::move(
                                                                                          source)),
                                                                                  trans(std::move(
                                                                                          trans)),
                                                                                  width(width),
                                                                                  height(height) {
}

void TileImage::setData(Data *data) {
    TileImage::data = data;
}

/* TODO
 * void Image::loadTexture(Renderer *renderer) {
    this->texture = renderer->loadTexture(this->source.c_str());
}
*/
