//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_GRID_H
#define ALITTLERPG_GRID_H

#include <string>

using namespace std;

class Grid {

public:
    Grid(const string &orientation, int width, int height);

private:
    string orientation;
    int width;
    int height;
};


#endif //ALITTLERPG_GRID_H
