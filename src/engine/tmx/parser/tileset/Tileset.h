//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_TILESET_H
#define ALITTLERPG_TILESET_H

#include "Grid.h"
#include "tile/TilesetTile.h"
#include "Tileset.h"
#include "TileOffset.h"
#include "TerrainTypes.h"
#include "wangset/Wangsets.h"

using namespace std;

class Tileset {

public:
    Tileset(int firstgid, string source, string name, int tileWidth, int tileHeight, int spacing,
            int margin, int tileCount, int columns);

    void setTileOffset(TileOffset *tileOffset);

    void setGrid(Grid *grid);

    void setProperties(Properties *properties);

    void setImage(TileImage *image);

    void setTerraintypes(TerrainTypes *terraintypes);

    vector<TilesetTile *> &getTiles();

    void setWangsets(Wangsets *wangsets);

    const string &getName() const;

    void setFirstgid(int firstgid);

    int getFirstgid() const;

    int firstgid = 0;

private:
    string source;
    string name;
    int tileWidth;
    int tileHeight;
    int spacing;
    int margin;
    int tileCount;
    int columns;

    TileOffset *tileOffset;
    Grid *grid;
    Properties *properties;
    TerrainTypes *terraintypes;
    TileImage *image;
    vector<TilesetTile *> tiles;
    Wangsets *wangsets;
};


#endif //ALITTLERPG_TILESET_H
