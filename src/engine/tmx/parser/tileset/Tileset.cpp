
//
// Created by Théophile Vieux on 2019-02-01.
//
#include "Tileset.h"

Tileset::Tileset(int firstgid, string source, string name, int tileWidth, int tileHeight, int spacing,
                 int margin, int tileCount, int columns) : firstgid(firstgid), source(std::move(source)),
                                                           name(std::move(name)),
                                                           tileWidth(tileWidth), tileHeight(tileHeight),
                                                           spacing(spacing), margin(margin), tileCount(tileCount),
                                                           columns(columns) {}

void Tileset::setImage(TileImage *image) {
    Tileset::image = image;
}

void Tileset::setGrid(Grid *grid) {
    Tileset::grid = grid;
}

void Tileset::setTileOffset(TileOffset *tileOffset) {
    Tileset::tileOffset = tileOffset;
}

void Tileset::setTerraintypes(TerrainTypes *terraintypes) {
    Tileset::terraintypes = terraintypes;
}

void Tileset::setProperties(Properties *properties) {
    Tileset::properties = properties;
}

vector<TilesetTile *> &Tileset::getTiles() {
    return this->tiles;
}

void Tileset::setWangsets(Wangsets *wangsets) {
    Tileset::wangsets = wangsets;
}

const string &Tileset::getName() const {
    return name;
}

void Tileset::setFirstgid(int firstgid) {
    Tileset::firstgid = firstgid;
}

int Tileset::getFirstgid() const {
    return this->firstgid;
}
