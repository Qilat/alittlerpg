//
// Created by Théophile Vieux on 2019-02-01.
//

#include "Terrain.h"

Terrain::Terrain(const std::string &name, int tile) : name(name), tile(tile) {}

void Terrain::setProperties(Properties *properties) {
    Terrain::properties = properties;
}
