//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_TILEOFFSET_H
#define ALITTLERPG_TILEOFFSET_H


class TileOffset {

public:
    TileOffset(int x, int y);

private:
    int x;
    int y;
};


#endif //ALITTLERPG_TILEOFFSET_H
