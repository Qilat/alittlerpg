//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_TERRAINTYPES_H
#define ALITTLERPG_TERRAINTYPES_H

#include "Terrain.h"

class TerrainTypes {

public:
    std::vector<Terrain *> &getTerrains();

private:
    std::vector<Terrain *> terrains;
};


#endif //ALITTLERPG_TERRAINTYPES_H
