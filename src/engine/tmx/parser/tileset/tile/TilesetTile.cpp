//
// Created by Théophile Vieux on 2019-02-01.
//

#include "TilesetTile.h"

TilesetTile::TilesetTile(int id, const string &type, const string &terrain, const string &probability) : id(id),
                                                                                                         type(type),
                                                                                                         terrain(terrain),
                                                                                                         probability(
                                                                                                                 probability) {}

void TilesetTile::setProperties(Properties *properties) {
    TilesetTile::properties = properties;
}

void TilesetTile::setImage(TileImage *image) {
    TilesetTile::image = image;
}

void TilesetTile::setObjectgroup(Objectgroup *objectgroup) {
    TilesetTile::objectgroup = objectgroup;
}

void TilesetTile::setAnimation(TileAnimation *animation) {
    TilesetTile::animation = animation;
}
