//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_TILE_H
#define ALITTLERPG_TILE_H

#include "../../objectgroup/Objectgroup.h"
#include "TileAnimation.h"

class TilesetTile {
public:
    TilesetTile(int id, const string &type, const string &terrain, const string &probability);

    void setProperties(Properties *properties);

    void setImage(TileImage *image);

    void setObjectgroup(Objectgroup *objectgroup);

    void setAnimation(TileAnimation *animation);

private:
    int id;
    std::string type;
    std::string terrain;
    std::string probability;

    Properties *properties;
    TileImage *image;
    Objectgroup *objectgroup;
    TileAnimation *animation;
};


#endif //ALITTLERPG_TILE_H
