//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_ANIMATION_H
#define ALITTLERPG_ANIMATION_H

#include <vector>
#include "Frame.h"

class TileAnimation {

public:
    std::vector<Frame *> &getFrames();

private:
    std::vector<Frame *> frames;
};


#endif //ALITTLERPG_ANIMATION_H
