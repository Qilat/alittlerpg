//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_FRAME_H
#define ALITTLERPG_FRAME_H


class Frame {

public:
    Frame(int tileId, int duration);

private:
    int tileId;
    int duration;
};


#endif //ALITTLERPG_FRAME_H
