//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_IMAGE_H
#define ALITTLERPG_IMAGE_H

#include "../layer/Data.h"

using namespace std;

class TileImage {

public:
    TileImage(string format, string source, string trans, int width, int height);

    void setData(Data *data);

    //void loadTexture(Renderer *renderer);

private:
    string format;
    string source;
    string trans;
    int width;
    int height;

    Data *data;

    //SDL_Texture *texture;

};


#endif //ALITTLERPG_IMAGE_H
