//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_TERRAIN_H
#define ALITTLERPG_TERRAIN_H

#include "../property/Properties.h"

class Terrain {

public:
    Terrain(const std::string &name, int tile);

    void setProperties(Properties *properties);

private:
    std::string name;
    int tile;
    Properties *properties;
};


#endif //ALITTLERPG_TERRAIN_H
