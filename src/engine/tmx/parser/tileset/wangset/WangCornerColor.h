//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_WANGCORNERCOLOR_H
#define ALITTLERPG_WANGCORNERCOLOR_H

#include <string>

using namespace std;

class WangCornerColor {

public:
    WangCornerColor(string name, string color, int tile, float probability);

private:
    string name;
    string color;
    int tile;
    float probability;

};


#endif //ALITTLERPG_WANGCORNERCOLOR_H
