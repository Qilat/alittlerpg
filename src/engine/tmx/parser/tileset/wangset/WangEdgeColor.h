//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_WANGEDGECOLOR_H
#define ALITTLERPG_WANGEDGECOLOR_H

#include <string>

using namespace std;

class WangEdgeColor {

public:
    WangEdgeColor(const string &name, const string &color, int tile, float probability);

private:
    string name;
    string color;
    int tile;
    float probability;
};


#endif //ALITTLERPG_WANGEDGECOLOR_H
