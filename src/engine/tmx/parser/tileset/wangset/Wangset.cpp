//
// Created by Théophile Vieux on 2019-02-01.
//

#include "Wangset.h"

Wangset::Wangset(const std::string &name, int tile) : name(name), tile(tile) {}

vector<WangCornerColor *> &Wangset::getWangCornerColors() {
    return wangCornerColors;
}

vector<WangEdgeColor *> &Wangset::getWangEdgeColors() {
    return wangEdgeColors;
}

vector<WangTile *> &Wangset::getWangTiles() {
    return wangTiles;
}
