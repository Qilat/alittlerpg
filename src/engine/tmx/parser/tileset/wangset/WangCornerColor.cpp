#include <utility>

//
// Created by Théophile Vieux on 2019-02-01.
//

#include "WangCornerColor.h"

WangCornerColor::WangCornerColor(string name, string color, int tile, float probability) : name(std::move(
        name)), color(std::move(color)), tile(tile), probability(probability) {}

