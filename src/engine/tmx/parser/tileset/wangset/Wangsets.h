//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_WANGSETS_H
#define ALITTLERPG_WANGSETS_H

#include "Wangset.h"

class Wangsets {

public:
    std::vector<Wangset *> &getWangsets();

private:
    std::vector<Wangset *> wangsets;
};


#endif //ALITTLERPG_WANGSETS_H
