//
// Created by Théophile Vieux on 2019-02-01.
//

#include "WangEdgeColor.h"

WangEdgeColor::WangEdgeColor(const string &name, const string &color, int tile, float probability) : name(name),
                                                                                                     color(color),
                                                                                                     tile(tile),
                                                                                                     probability(
                                                                                                             probability) {

}
