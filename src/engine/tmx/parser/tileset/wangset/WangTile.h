//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_WANGTILE_H
#define ALITTLERPG_WANGTILE_H


class WangTile {
public:
    WangTile(int tileid, int wangid);

private:
    int tileid;
    int wangid;
};


#endif //ALITTLERPG_WANGTILE_H
