//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_WANGSET_H
#define ALITTLERPG_WANGSET_H

#include <vector>
#include "WangCornerColor.h"
#include "WangEdgeColor.h"
#include "WangTile.h"

class Wangset {

public:
    Wangset(const std::string &name, int tile);

    vector<WangCornerColor *> &getWangCornerColors();

    vector<WangEdgeColor *> &getWangEdgeColors();

    vector<WangTile *> &getWangTiles();

private:
    std::string name;
    int tile;

    std::vector<WangCornerColor *> wangCornerColors;
    std::vector<WangEdgeColor *> wangEdgeColors;
    std::vector<WangTile *> wangTiles;

};


#endif //ALITTLERPG_WANGSET_H
