//
// Created by Théophile Vieux on 2019-02-03.
//

#ifndef ALITTLERPG_IMAGELAYER_H
#define ALITTLERPG_IMAGELAYER_H

#include <string>
#include "property/Properties.h"
#include "tileset/TileImage.h"

using namespace std;

class ImageLayer {

public:
    ImageLayer(int id, const string &name, int offsetx, int offsety, int x, int y, float opacity, int visible);

    void setProperties(Properties *properties);

    vector<TileImage *> &getImages();

private:
    int id;
    string name;
    int offsetx;
    int offsety;
    int x;
    int y;
    float opacity;
    int visible;

    Properties *properties;
    vector<TileImage *> images;
};


#endif //ALITTLERPG_IMAGELAYER_H
