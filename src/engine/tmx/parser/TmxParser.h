//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_TMXLOADER_H
#define ALITTLERPG_TMXLOADER_H

#include <iostream>

#include "../../../../libs/pugixml/src/pugixml.hpp"
#include "Tilemap.h"


using namespace pugi;
using namespace std;

class TmxParser {

public:
    static Tilemap *loadTilemapFromTMXFile(char *path);

    static Tileset *loadTilesetFromTSXFile(char *path);

    const string &getRootPath() const;

    const string &getDocName() const;

private:

    string rootPath;
    string docName;

    static xml_document *loadXmlDocument(char *path);

    explicit TmxParser(char *path);

    Tilemap *parseMapNode(xml_node node);

    Tileset *parseTilesetNode(xml_node node);

    Grid *parseGridNode(xml_node node);

    TileOffset *parseTileOffsetNode(xml_node node);

    TileImage *parseImageNode(xml_node node);

    Data *parseDataNode(xml_node node);

    TilesetTile *parseTilesetTileNode(xml_node node);

    Chunk *parseChunkNode(xml_node node);

    Properties *parsePropertiesNode(xml_node node);

    Property *parsePropertyNode(xml_node node);

    TerrainTypes *parseTerraintypesNode(xml_node node);

    Terrain *parseTerrainNode(xml_node node);

    Objectgroup *parseObjectgroupNode(xml_node node);

    TileAnimation *parseAnimationNode(xml_node node);

    Frame *parseFrameNode(xml_node node);

    Wangsets *parseWangsetsNode(xml_node node);

    Wangset *parseWangsetNode(xml_node node);

    WangCornerColor *parseWangCornerColorNode(xml_node node);

    WangEdgeColor *parseWangEdgeColorNode(xml_node node);

    WangTile *parseWangTileNode(xml_node node);

    Layer *parseLayerNode(xml_node node);

    LayerTile *parseLayerTileNode(xml_node node);

    Object *parseObjectNode(xml_node node);

    Ellipse *parseEllipseNode(xml_node node);

    Polygon *parsePolygonNode(xml_node node);

    Polyline *parsePolylineNode(xml_node node);

    Text *parseTextNode(xml_node node);

    Point *parsePointNode(xml_node node);

    ImageLayer *parseImageLayerNode(xml_node node);

    Group *parseGroupNode(xml_node node);
};

#endif //ALITTLERPG_TMXLOADER_H
