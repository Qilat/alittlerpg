//
// Created by Théophile Vieux on 2019-02-01.
//

#include "Tilemap.h"

Tilemap::Tilemap(string version, string tiledVersion, string orientation, string renderorder,
                 int width, int height, int tileWidth, int tileHeight, int hexSideLength, string staggerAxis,
                 int staggerIndex,
                 string backgroundColor, int nextLayerId, int nextObjectId) : version(std::move(version)),
                                                                              tiledVersion(std::move(tiledVersion)),
                                                                              orientation(std::move(orientation)),
                                                                              renderorder(std::move(renderorder)),
                                                                              width(width),
                                                                              height(height), tileWidth(tileWidth),
                                                                              tileHeight(tileHeight),
                                                                              hexSideLength(hexSideLength),
                                                                              staggerAxis(std::move(staggerAxis)),
                                                                              staggerIndex(staggerIndex),
                                                                              backgroundColor(
                                                                                      std::move(backgroundColor)),
                                                                              nextLayerId(nextLayerId),
                                                                              nextObjectId(nextObjectId) {
}

vector<Layer *> &Tilemap::getLayers() {
    return this->layers;
}

vector<Tileset *> &Tilemap::getTileSets() {
    return this->tileSets;
}

void Tilemap::setProperties(Properties *properties) {
    Tilemap::properties = properties;
}

vector<ImageLayer *> &Tilemap::getImageLayers() {
    return this->imagesLayers;
}

vector<Objectgroup *> &Tilemap::getObjectgroups() {
    return this->objectgroups;
}

vector<Group *> &Tilemap::getGroups() {
    return this->groups;
}

const string &Tilemap::getVersion() const {
    return version;
}
