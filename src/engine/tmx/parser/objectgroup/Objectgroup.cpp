//
// Created by Théophile Vieux on 2019-02-01.
//

#include "Objectgroup.h"

Objectgroup::Objectgroup(int id, const string &name, const string &color, int x, int y, int width, int height,
                         float opacity, int visibility, int offsetx, int offsety, const string &draworder) : id(id),
                                                                                                             name(name),
                                                                                                             color(color),
                                                                                                             x(x), y(y),
                                                                                                             width(width),
                                                                                                             height(height),
                                                                                                             opacity(opacity),
                                                                                                             visibility(
                                                                                                                     visibility),
                                                                                                             offsetx(offsetx),
                                                                                                             offsety(offsety),
                                                                                                             draworder(
                                                                                                                     draworder) {}

void Objectgroup::setProperties(Properties *properties) {
    Objectgroup::properties = properties;
}

vector<Object *> &Objectgroup::getObjects() {
    return objects;
}
