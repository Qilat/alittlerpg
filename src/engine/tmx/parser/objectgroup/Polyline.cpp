//
// Created by Théophile Vieux on 2019-02-03.
//

#include "Polyline.h"

Polyline::Polyline() = default;

vector<Point *> &Polyline::getPoints() {
    return this->points;
}

