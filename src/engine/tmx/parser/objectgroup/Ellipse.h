//
// Created by Théophile Vieux on 2019-02-03.
//

#ifndef ALITTLERPG_ELLIPSE_H
#define ALITTLERPG_ELLIPSE_H


class Ellipse {

public:
    Ellipse(int x, int y, int width, int height);

private:
    int x;
    int y;
    int width;
    int height;
};


#endif //ALITTLERPG_ELLIPSE_H
