//
// Created by Théophile Vieux on 2019-02-03.
//

#ifndef ALITTLERPG_POLYLINE_H
#define ALITTLERPG_POLYLINE_H

#include <vector>
#include "Point.h"

using namespace std;

class Polyline {
public:
    Polyline();

    vector<Point *> &getPoints();

private:
    vector<Point *> points;
};


#endif //ALITTLERPG_POLYLINE_H
