//
// Created by Théophile Vieux on 2019-02-03.
//

#ifndef ALITTLERPG_POINT_H
#define ALITTLERPG_POINT_H


class Point {

public:
    Point(int x, int y);

private:
    int x;
    int y;
};


#endif //ALITTLERPG_POINT_H
