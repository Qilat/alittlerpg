//
// Created by Théophile Vieux on 2019-02-03.
//

#ifndef ALITTLERPG_POLYGON_H
#define ALITTLERPG_POLYGON_H

#include <vector>
#include "Point.h"

using namespace std;

class Polygon {
public:
    Polygon();

    vector<Point *> &getPoints();

private:
    vector<Point *> points;
};


#endif //ALITTLERPG_POLYGON_H
