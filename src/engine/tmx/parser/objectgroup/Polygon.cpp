//
// Created by Théophile Vieux on 2019-02-03.
//

#include "Polygon.h"

Polygon::Polygon() = default;

vector<Point *> &Polygon::getPoints() {
    return this->points;
}
