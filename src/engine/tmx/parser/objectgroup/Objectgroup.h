//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_OBJECTGROUP_H
#define ALITTLERPG_OBJECTGROUP_H

#include "Object.h"

using namespace std;

class Objectgroup {


public:
    Objectgroup(int id, const string &name, const string &color, int x, int y, int width, int height, float opacity,
                int visibility, int offsetx, int offsety, const string &draworder);

    void setProperties(Properties *properties);

    vector<Object *> &getObjects();

private:
    int id;
    string name;
    string color;
    int x;
    int y;
    int width;
    int height;
    float opacity;
    int visibility;
    int offsetx;
    int offsety;
    string draworder;

    Properties *properties;
    vector<Object *> objects;

};


#endif //ALITTLERPG_OBJECTGROUP_H
