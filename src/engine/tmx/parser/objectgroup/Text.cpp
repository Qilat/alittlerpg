//
// Created by Théophile Vieux on 2019-02-03.
//

#include "Text.h"

Text::Text(const string &fontfamily, int pixelsize, int wrap, const string &color, int bold, int italic, int underline,
           int strikeout, int kerning, const string &halign, const string &valign, const string &value) : fontfamily(
        fontfamily), pixelsize(pixelsize), wrap(wrap), color(color), bold(bold), italic(italic), underline(underline),
                                                                                                          strikeout(
                                                                                                                  strikeout),
                                                                                                          kerning(kerning),
                                                                                                          halign(halign),
                                                                                                          valign(valign),
                                                                                                          value(value) {}
