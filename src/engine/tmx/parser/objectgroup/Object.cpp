//
// Created by Théophile Vieux on 2019-02-02.
//

#include "Object.h"

Object::Object(int id, const string &name, const string &type, int x, int y, int width, int height, int rotation,
               int gid, int visible, const string &templateRef) : id(id), name(name), type(type), x(x), y(y),
                                                                  width(width), height(height), rotation(rotation),
                                                                  gid(gid), visible(visible),
                                                                  templateRef(templateRef) {}

void Object::setProperties(Properties *properties) {
    Object::properties = properties;
}

vector<Ellipse *> &Object::getEllipses() {
    return ellipses;
}

vector<Polygon *> &Object::getPolygons() {
    return polygons;
}

vector<Polyline *> &Object::getPolylines() {
    return polylines;
}

vector<Text *> &Object::getTexts() {
    return texts;
}

vector<TileImage *> &Object::getImages() {
    return images;
}
