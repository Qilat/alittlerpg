//
// Created by Théophile Vieux on 2019-02-03.
//

#ifndef ALITTLERPG_TEXT_H
#define ALITTLERPG_TEXT_H

#include <string>

using namespace std;

class Text {

public:
    Text(const string &fontfamily, int pixelsize, int wrap, const string &color, int bold, int italic, int underline,
         int strikeout, int kerning, const string &halign, const string &valign, const string &value);

private:
    string fontfamily;
    int pixelsize;
    int wrap;
    string color;
    int bold;
    int italic;
    int underline;
    int strikeout;
    int kerning;
    string halign;
    string valign;

    string value;
};


#endif //ALITTLERPG_TEXT_H
