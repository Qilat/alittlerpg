//
// Created by Théophile Vieux on 2019-02-02.
//

#ifndef ALITTLERPG_OBJECT_H
#define ALITTLERPG_OBJECT_H

#include "../property/Properties.h"
#include "Ellipse.h"
#include "Polygon.h"
#include "Polyline.h"
#include "Text.h"
#include "../tileset/TileImage.h"

using namespace std;

class Object {

public:
    Object(int id, const string &name, const string &type, int x, int y, int width, int height, int rotation, int gid,
           int visible, const string &templateRef);

    void setProperties(Properties *properties);

    vector<Ellipse *> &getEllipses();

    vector<Polygon *> &getPolygons();

    vector<Polyline *> &getPolylines();

    vector<Text *> &getTexts();

    vector<TileImage *> &getImages();

private:
    int id;
    string name;
    string type;
    int x;
    int y;
    int width;
    int height;
    int rotation;
    int gid;
    int visible;
    string templateRef;

    Properties *properties;
    vector<Ellipse *> ellipses;
    vector<Polygon *> polygons;
    vector<Polyline *> polylines;
    vector<Text *> texts;
    vector<TileImage *> images;

};


#endif //ALITTLERPG_OBJECT_H
