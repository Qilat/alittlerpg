//
// Created by Théophile Vieux on 2019-02-01.
//

#ifndef ALITTLERPG_MAP_H
#define ALITTLERPG_MAP_H

#include "tileset/Tileset.h"
#include "Group.h"

using namespace std;

class Tilemap {

public:
    Tilemap(string version, string tiledVersion, string orientation, string renderorder,
            int width, int height, int tileWidth, int tileHeight, int hexSideLength, string staggerAxis,
            int staggerIndex,
            string backgroundColor, int nextLayerId, int nextObjectId);

    vector<Tileset *> &getTileSets();

    vector<Layer *> &getLayers();

    vector<ImageLayer *> &getImageLayers();

    vector<Objectgroup *> &getObjectgroups();

    vector<Group *> &getGroups();

    void setProperties(Properties *properties);

    const string &getVersion() const;

private:
    string version;
    string tiledVersion;
    string orientation;
    string renderorder;
    int width;
    int height;
    int tileWidth;
    int tileHeight;
    int hexSideLength;
    string staggerAxis;
    int staggerIndex;
    string backgroundColor;
    int nextLayerId;
    int nextObjectId;

    Properties *properties;

    vector<Tileset *> tileSets;
    vector<Layer *> layers;
    vector<ImageLayer *> imagesLayers;
    vector<Objectgroup *> objectgroups;
    vector<Group *> groups;

};


#endif //ALITTLERPG_MAP_H
