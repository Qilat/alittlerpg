//
// Created by Théophile Vieux on 2019-02-03.
//

#include "Group.h"

Group::Group(int id, const string &name, int offsetx, int offsety, float opacity, int visible) : id(id), name(name),
                                                                                                 offsetx(offsetx),
                                                                                                 offsety(offsety),
                                                                                                 opacity(opacity),
                                                                                                 visible(visible) {}

void Group::setProperties(Properties *properties) {
    Group::properties = properties;
}

std::vector<Layer *> &Group::getLayers() {
    return layers;
}

std::vector<Objectgroup *> &Group::getObjectGroups() {
    return objectGroups;
}

std::vector<ImageLayer *> &Group::getImageLayers() {
    return imageLayers;
}

std::vector<Group *> &Group::getGroups() {
    return groups;
}
