//
// Created by Théophile Vieux on 2019-02-03.
//

#include "ImageLayer.h"

ImageLayer::ImageLayer(int id, const string &name, int offsetx, int offsety, int x, int y, float opacity, int visible)
        : id(id), name(name), offsetx(offsetx), offsety(offsety), x(x), y(y), opacity(opacity), visible(visible) {}

void ImageLayer::setProperties(Properties *properties) {
    ImageLayer::properties = properties;
}

vector<TileImage *> &ImageLayer::getImages() {
    return this->images;
}
