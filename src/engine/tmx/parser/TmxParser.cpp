//
// Created by Théophile Vieux on 2019-02-01.
//

#include "TmxParser.h"

Tilemap *TmxParser::loadTilemapFromTMXFile(char *path) {
    auto parser = new TmxParser(path);
    xml_document *doc = TmxParser::loadXmlDocument(path);
    xml_node mapNode = doc->child("map");
    if (mapNode)
        return parser->parseMapNode(mapNode);

    cout << "File not found or invalid file: " + *(new string(path)) << endl;
    exit(EXIT_FAILURE);
}

Tileset *TmxParser::loadTilesetFromTSXFile(char *path) {
    auto parser = new TmxParser(path);
    xml_document *doc = TmxParser::loadXmlDocument(path);
    xml_node tilesetNode = doc->child("tileset");
    if (tilesetNode)
        return parser->parseTilesetNode(tilesetNode);

    cout << "File not found or invalid file: " + *(new string(path)) << endl;
    exit(EXIT_FAILURE);
}

xml_document *TmxParser::loadXmlDocument(char *path) {
    auto doc = new xml_document();
    doc->load_file(path);
    return doc;
}

Tilemap *TmxParser::parseMapNode(xml_node node) {
    Tilemap *map = new Tilemap(
            node.attribute("version").as_string(),
            node.attribute("tiledversion").as_string(),
            node.attribute("orientation").as_string(),
            node.attribute("renderorder").as_string(),
            node.attribute("width").as_int(),
            node.attribute("height").as_int(),
            node.attribute("tilewidth").as_int(),
            node.attribute("tileheight").as_int(),
            node.attribute("hexsidelength").as_int(),
            node.attribute("staggerAxis").as_string(),
            node.attribute("staggerindex").as_int(),
            node.attribute("backgroundcolor").as_string(),
            node.attribute("nextlayerid").as_int(),
            node.attribute("nextobjectid").as_int());

    xml_node propertiesNode = node.child("properties");
    if (propertiesNode)
        map->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

    for (xml_node layerNode : node.children("layer")) {
        map->getLayers().push_back(TmxParser::parseLayerNode(layerNode));
    }

    for (xml_node objectgroupNode : node.children("objectgroup")) {
        map->getObjectgroups().push_back(TmxParser::parseObjectgroupNode(objectgroupNode));
    }

    for (xml_node imageLayerNode : node.children("imagelayer")) {
        map->getImageLayers().push_back(TmxParser::parseImageLayerNode(imageLayerNode));
    }

    for (xml_node groupNode : node.children("group")) {
        map->getGroups().push_back(TmxParser::parseGroupNode(groupNode));
    }

    for (xml_node tileSetNode : node.children("tileset")) {
        map->getTileSets().push_back(TmxParser::parseTilesetNode(tileSetNode));
    }

    return map;
}

Tileset *TmxParser::parseTilesetNode(xml_node node) {
    string source = node.attribute("source").as_string();
    Tileset *tileset;
    if (!source.empty()) {

        source = this->getRootPath() + source;
        tileset = TmxParser::loadTilesetFromTSXFile(const_cast<char *>(source.c_str()));
        if (tileset == nullptr) {
            cout << "File not found : " + source << endl;
            exit(EXIT_FAILURE);
        }
        tileset->setFirstgid(node.attribute("firstgid").as_int());
    } else {
        tileset = new Tileset(
                node.attribute("firstgid").as_int(),
                node.attribute("source").as_string(),
                node.attribute("name").as_string(),
                node.attribute("tilewidth").as_int(),
                node.attribute("tileheight").as_int(),
                node.attribute("spacing").as_int(),
                node.attribute("margin").as_int(),
                node.attribute("tilecount").as_int(),
                node.attribute("columns").as_int()
        );

        xml_node tileoffsetNode = node.child("tileoffset");
        if (tileoffsetNode)
            tileset->setTileOffset(TmxParser::parseTileOffsetNode(tileoffsetNode));

        xml_node gridNode = node.child("grid");
        if (gridNode)
            tileset->setGrid(TmxParser::parseGridNode(gridNode));

        xml_node propertiesNode = node.child("properties");
        if (gridNode)
            tileset->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

        xml_node imageNode = node.child("image");
        if (imageNode)
            tileset->setImage(TmxParser::parseImageNode(imageNode));

        xml_node terraintypesNode = node.child("terraintypes");
        if (terraintypesNode)
            tileset->setTerraintypes(TmxParser::parseTerraintypesNode(terraintypesNode));

        for (xml_node tileNode : node.children("tile"))
            tileset->getTiles().push_back(TmxParser::parseTilesetTileNode(tileNode));

        xml_node wangsetsNode = node.child("wangsetsNode");
        if (wangsetsNode)
            tileset->setWangsets(TmxParser::parseWangsetsNode(wangsetsNode));

    }
    return tileset;
}

Grid *TmxParser::parseGridNode(xml_node node) {
    return new Grid(
            node.attribute("orientation").as_string(),
            node.attribute("width").as_int(),
            node.attribute("height").as_int()
    );
}

TileOffset *TmxParser::parseTileOffsetNode(xml_node node) {
    return new TileOffset(
            node.attribute("x").as_int(),
            node.attribute("y").as_int()
    );
}

TileImage *TmxParser::parseImageNode(xml_node node) {
    TileImage *image = new TileImage(
            node.attribute("format").as_string(),
            this->rootPath + node.attribute("source").as_string(),
            node.attribute("trans").as_string(),
            node.attribute("width").as_int(),
            node.attribute("height").as_int()
    );

    xml_node dataNode = node.child("data");
    if (dataNode)
        image->setData(TmxParser::parseDataNode(dataNode));

    return image;
}

Data *TmxParser::parseDataNode(xml_node node) {
    Data *data = new Data(
            node.attribute("encoding").as_string(),
            node.attribute("compression").as_string(),
            node.text().as_string()
    );

    for (xml_node tileNode : node.children("tile"))
        data->getTiles().push_back(TmxParser::parseLayerTileNode(tileNode));

    for (xml_node chunkNode : node.children("chunk"))
        data->getChunks().push_back(TmxParser::parseChunkNode(chunkNode));

    return data;
}

TilesetTile *TmxParser::parseTilesetTileNode(xml_node node) {
    auto tile = new TilesetTile(
            node.attribute("id").as_int(),
            node.attribute("type").as_string(),
            node.attribute("terrain").as_string(),
            node.attribute("probability").as_string()
    );

    xml_node propertiesNode = node.child("properties");
    if (propertiesNode)
        tile->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

    xml_node imageNode = node.child("image");
    if (imageNode)
        tile->setImage(TmxParser::parseImageNode(imageNode));

    xml_node objectgroupNode = node.child("objectgroup");
    if (objectgroupNode)
        tile->setObjectgroup(TmxParser::parseObjectgroupNode(objectgroupNode));

    xml_node animationNode = node.child("animation");
    if (animationNode)
        tile->setAnimation(TmxParser::parseAnimationNode(animationNode));

    return tile;
}

Chunk *TmxParser::parseChunkNode(xml_node node) {
    auto chunk = new Chunk(
            node.attribute("x").as_int(),
            node.attribute("y").as_int(),
            node.attribute("width").as_int(),
            node.attribute("height").as_int()
    );

    for (xml_node tileNode : node.children("tile"))
        chunk->getTiles().push_back(TmxParser::parseLayerTileNode(tileNode));

    return chunk;
}

Properties *TmxParser::parsePropertiesNode(xml_node node) {
    auto properties = new Properties();

    for (xml_node propertyNode : node.children("property"))
        properties->getProperties().push_back(TmxParser::parsePropertyNode(propertyNode));

    return properties;
}

Property *TmxParser::parsePropertyNode(xml_node node) {
    return new Property(
            node.attribute("name").as_string(),
            node.attribute("type").as_string(),
            node.attribute("value").as_string()
    );
}

TerrainTypes *TmxParser::parseTerraintypesNode(xml_node node) {
    auto terraintypes = new TerrainTypes();

    for (xml_node terrainNode : node.children("terrain"))
        terraintypes->getTerrains().push_back(TmxParser::parseTerrainNode(terrainNode));

    return terraintypes;
}

Terrain *TmxParser::parseTerrainNode(xml_node node) {
    auto terrain = new Terrain(
            node.attribute("name").as_string(),
            node.attribute("tile").as_int()
    );

    xml_node propertiesNode = node.child("properties");
    if (propertiesNode)
        terrain->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

    return terrain;
}

TileAnimation *TmxParser::parseAnimationNode(xml_node node) {
    auto animation = new TileAnimation();

    for (xml_node frameNode : node.children("frame"))
        animation->getFrames().push_back(TmxParser::parseFrameNode(frameNode));

    return animation;
}

Frame *TmxParser::parseFrameNode(xml_node node) {
    return new Frame(
            node.attribute("tileid").as_int(),
            node.attribute("duration").as_int()
    );
}

Wangsets *TmxParser::parseWangsetsNode(xml_node node) {
    auto wangsets = new Wangsets();

    for (xml_node wangsetNode : node.children("wangset"))
        wangsets->getWangsets().push_back(TmxParser::parseWangsetNode(wangsetNode));

    return wangsets;
}

Wangset *TmxParser::parseWangsetNode(xml_node node) {
    auto wangset = new Wangset(
            node.attribute("name").as_string(),
            node.attribute("tile").as_int()
    );

    for (xml_node wangcornercolorNode : node.children("wangcornercolor"))
        wangset->getWangCornerColors().push_back(TmxParser::parseWangCornerColorNode(wangcornercolorNode));

    for (xml_node wangedgecolorNode : node.children("wangedgecolor"))
        wangset->getWangEdgeColors().push_back(TmxParser::parseWangEdgeColorNode(wangedgecolorNode));

    for (xml_node wangtileNode : node.children("wangtile"))
        wangset->getWangTiles().push_back(TmxParser::parseWangTileNode(wangtileNode));

    return wangset;
}

WangCornerColor *TmxParser::parseWangCornerColorNode(xml_node node) {
    return new WangCornerColor(
            node.attribute("name").as_string(),
            node.attribute("color").as_string(),
            node.attribute("tile").as_int(),
            node.attribute("probability").as_float());
}

WangEdgeColor *TmxParser::parseWangEdgeColorNode(xml_node node) {
    return new WangEdgeColor(
            node.attribute("name").as_string(),
            node.attribute("color").as_string(),
            node.attribute("tile").as_int(),
            node.attribute("probability").as_float()
    );
}

WangTile *TmxParser::parseWangTileNode(xml_node node) {
    return new WangTile(
            node.attribute("tileid").as_int(),
            node.attribute("wangid").as_int()
    );
}

Layer *TmxParser::parseLayerNode(xml_node node) {
    auto layer = new Layer(
            node.attribute("id").as_int(),
            node.attribute("name").as_string(),
            node.attribute("x").as_int(),
            node.attribute("y").as_int(),
            node.attribute("width").as_int(),
            node.attribute("height").as_int(),
            node.attribute("opacity").as_float(),
            node.attribute("visible").as_int(),
            node.attribute("offsetx").as_int(),
            node.attribute("offsety").as_int()
    );

    xml_node propertiesNode = node.child("properties");
    if (propertiesNode)
        layer->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

    xml_node dataNode = node.child("data");
    if (dataNode)
        layer->setData(TmxParser::parseDataNode(dataNode));

    return layer;
}

Objectgroup *TmxParser::parseObjectgroupNode(xml_node node) {
    auto objectgroup = new Objectgroup(
            node.attribute("id").as_int(),
            node.attribute("name").as_string(),
            node.attribute("color").as_string(),
            node.attribute("x").as_int(),
            node.attribute("y").as_int(),
            node.attribute("width").as_int(),
            node.attribute("height").as_int(),
            node.attribute("opacity").as_float(),
            node.attribute("visibility").as_int(),
            node.attribute("offsetx").as_int(),
            node.attribute("offsety").as_int(),
            node.attribute("draworder").as_string()
    );

    xml_node propertiesNode = node.child("properties");
    if (propertiesNode)
        objectgroup->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

    for (xml_node objectNode : node.children("object"))
        objectgroup->getObjects().push_back(TmxParser::parseObjectNode(objectNode));


    return objectgroup;
}

Object *TmxParser::parseObjectNode(xml_node node) {
    Object *object = new Object(
            node.attribute("id").as_int(),
            node.attribute("name").as_string(),
            node.attribute("type").as_string(),
            node.attribute("x").as_int(),
            node.attribute("y").as_int(),
            node.attribute("width").as_int(),
            node.attribute("height").as_int(),
            node.attribute("rotation").as_int(),
            node.attribute("gid").as_int(),
            node.attribute("visible").as_int(),
            node.attribute("template").as_string()
    );


    xml_node propertiesNode = node.child("properties");
    if (propertiesNode)
        object->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

    for (xml_node ellipseNode : node.children("ellipse")) {
        object->getEllipses().push_back(TmxParser::parseEllipseNode(ellipseNode));
    }

    for (xml_node polygonNode : node.children("polygon")) {
        object->getPolygons().push_back(TmxParser::parsePolygonNode(polygonNode));
    }

    for (xml_node polylineNode : node.children("polyline")) {
        object->getPolylines().push_back(TmxParser::parsePolylineNode(polylineNode));
    }

    for (xml_node textNode : node.children("text")) {
        object->getTexts().push_back(TmxParser::parseTextNode(textNode));
    }

    for (xml_node imageNode : node.children("image")) {
        object->getImages().push_back(TmxParser::parseImageNode(imageNode));
    }

    return object;
}

LayerTile *TmxParser::parseLayerTileNode(xml_node node) {
    return new LayerTile(node.attribute("gid").as_int());
}

Ellipse *TmxParser::parseEllipseNode(xml_node node) {
    return new Ellipse(
            node.attribute("x").as_int(),
            node.attribute("y").as_int(),
            node.attribute("width").as_int(),
            node.attribute("height").as_int()
    );
}

Polygon *TmxParser::parsePolygonNode(xml_node node) {
    auto polygon = new Polygon();

    for (xml_node pointNode : node.children("point"))
        polygon->getPoints().push_back(TmxParser::parsePointNode(pointNode));

    return polygon;
}

Polyline *TmxParser::parsePolylineNode(xml_node node) {

    auto polyline = new Polyline();

    for (xml_node pointNode : node.children("point"))
        polyline->getPoints().push_back(TmxParser::parsePointNode(pointNode));

    return polyline;
}

Text *TmxParser::parseTextNode(xml_node node) {
    return new Text(
            node.attribute("fontfamily").as_string(),
            node.attribute("pixelsize").as_int(),
            node.attribute("wrap").as_int(),
            node.attribute("color").as_string(),
            node.attribute("bold").as_int(),
            node.attribute("italic").as_int(),
            node.attribute("underline").as_int(),
            node.attribute("strikeout").as_int(),
            node.attribute("kerning").as_int(),
            node.attribute("halign").as_string(),
            node.attribute("valign").as_string(),
            node.value()
    );
}

Point *TmxParser::parsePointNode(xml_node node) {
    return new Point(
            node.attribute("x").as_int(),
            node.attribute("y").as_int()
    );
}

ImageLayer *TmxParser::parseImageLayerNode(xml_node node) {
    ImageLayer *imageLayer = new ImageLayer(
            node.attribute("id").as_int(),
            node.attribute("name").as_string(),
            node.attribute("offsetx").as_int(),
            node.attribute("offsety").as_int(),
            node.attribute("x").as_int(),
            node.attribute("y").as_int(),
            node.attribute("opacity").as_float(),
            node.attribute("visible").as_int()
    );

    xml_node propertiesNode = node.child("properties");
    if (propertiesNode)
        imageLayer->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

    for (xml_node imageNode : node.children("image"))
        imageLayer->getImages().push_back(TmxParser::parseImageNode(imageNode));

    return imageLayer;
}

Group *TmxParser::parseGroupNode(xml_node node) {
    Group *group = new Group(
            node.attribute("id").as_int(),
            node.attribute("name").as_string(),
            node.attribute("offsetx").as_int(),
            node.attribute("offsety").as_int(),
            node.attribute("opacity").as_float(),
            node.attribute("visible").as_int()
    );

    xml_node propertiesNode = node.child("properties");
    if (propertiesNode)
        group->setProperties(TmxParser::parsePropertiesNode(propertiesNode));

    for (xml_node layerNode : node.children("layer"))
        group->getLayers().push_back(TmxParser::parseLayerNode(layerNode));

    for (xml_node objectgroupNode : node.children("objectgroup"))
        group->getObjectGroups().push_back(TmxParser::parseObjectgroupNode(objectgroupNode));

    for (xml_node imagelayerNode : node.children("imagelayer"))
        group->getImageLayers().push_back(TmxParser::parseImageLayerNode(imagelayerNode));

    for (xml_node groupNode : node.children("group"))
        group->getGroups().push_back(TmxParser::parseGroupNode(groupNode));

    return group;
}

TmxParser::TmxParser(char *path) {
    auto pathStr = new string(path);
    unsigned long lastIndex = pathStr->find_last_of("/");

    if (pathStr->find("/") != std::string::npos) {
        rootPath = pathStr->substr(0, lastIndex + 1);
        docName = pathStr->substr(lastIndex - 1, pathStr->size());
    } else {
        rootPath = "";
        docName = *pathStr;
    }

}

const string &TmxParser::getRootPath() const {
    return rootPath;
}

const string &TmxParser::getDocName() const {
    return docName;
}
