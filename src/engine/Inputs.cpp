//
// Created by Théophile Vieux on 2019-01-16.
//

#include "Inputs.h"

void Inputs::updateInputs() {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT)
            this->quit = SDL_TRUE;
        else if (event.type == SDL_KEYDOWN)
            this->key[event.key.keysym.scancode] = SDL_TRUE;
        else if (event.type == SDL_KEYUP)
            this->key[event.key.keysym.scancode] = SDL_FALSE;
        else if (event.type == SDL_MOUSEMOTION) {
            this->mouseX = event.motion.x;
            this->mouseY = event.motion.y;
            this->deltaMouseX = event.motion.xrel;
            this->deltaMouseY = event.motion.yrel;
        } else if (event.type == SDL_MOUSEWHEEL) {
            this->xwheel = event.wheel.x;
            this->ywheel = event.wheel.y;
        } else if (event.type == SDL_MOUSEBUTTONDOWN)
            this->mouse[event.button.button] = SDL_TRUE;
        else if (event.type == SDL_MOUSEBUTTONUP)
            this->mouse[event.button.button] = SDL_FALSE;
    }
}
