#include <iostream>
#include "ALittleRPGConfig.h"
#include "rpg/StageHandler.h"

using namespace std;

int main() {
    string version = to_string(ALITTLERPG_VERSION_MAJOR) + "." + to_string(ALITTLERPG_VERSION_MINOR);
    cout << "Starting ALittleRPG " + version << "..." << endl;

    if (SDL_Init(SDL_INIT_VIDEO) == -1)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    if (IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) == -1) {
        fprintf(stderr, "Erreur d'initialisation de la SDL_image : %s\n", IMG_GetError());
        exit(EXIT_FAILURE);
    }

    if (TTF_Init() == -1) {
        fprintf(stderr, "Erreur d'initialisation de la SDL_ttf : %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }
    GameEngine *gameEngine = new GameEngine("Sacha Touille v" + version, 1080, 920);

    new StageHandler(gameEngine);
    StageHandler::showMainMenu();

    gameEngine->run();

    TTF_Quit();
    IMG_Quit();
    SDL_Quit();

    return 0;
}
