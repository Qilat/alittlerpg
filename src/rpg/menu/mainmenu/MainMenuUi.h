//
// Created by Théophile Vieux on 2019-01-22.
//

#ifndef ALITTLERPG_MAINMENUUI_H
#define ALITTLERPG_MAINMENUUI_H

#include "../../../engine/stage/Ui.h"
#include "../../../engine/element/ui/image/Image.h"
#include "MainMenuButton.h"
#include "../../StageHandler.h"

class MainMenuUi : public Ui {
public:
    void init(Window *window) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;


private:
    Image *backgroundImage = nullptr;
    Image *bannerImage = nullptr;

    Button *singleplayerButton = nullptr;
    Button *multiplayerButton = nullptr;
    Button *settingsButton = nullptr;
    Button *quitButton = nullptr;
};


#endif //ALITTLERPG_MAINMENUUI_H
