//
// Created by Théophile Vieux on 2019-01-21.
//

#ifndef ALITTLERPG_MAINMENUBUTTON_H
#define ALITTLERPG_MAINMENUBUTTON_H

#include "../../../engine/element/ui/button/ColoredButton.h"

class MainMenuButton : public ColoredButton {
public:
    MainMenuButton(int id, char *text);

    MainMenuButton(int id, char *text, SDL_Color *color);

protected:
    void drawHover(Renderer *renderer) override;

    void drawNormal(Renderer *renderer) override;

    void drawClicked(Renderer *renderer) override;

private:
    void drawColoredButton(Renderer *renderer, Uint8 delta);
};


#endif //ALITTLERPG_MAINMENUBUTTON_H
