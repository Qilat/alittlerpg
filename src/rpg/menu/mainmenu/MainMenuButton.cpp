//
// Created by Théophile Vieux on 2019-01-21.
//

#include "MainMenuButton.h"

MainMenuButton::MainMenuButton(int id, char *text) : ColoredButton(id, text) {}

MainMenuButton::MainMenuButton(int id, char *text, SDL_Color *color) : ColoredButton(id, text, color) {}

void MainMenuButton::drawNormal(Renderer *renderer) {
    this->drawColoredButton(renderer, 0);
}

void MainMenuButton::drawHover(Renderer *renderer) {
    this->drawColoredButton(renderer, 20);
}

void MainMenuButton::drawClicked(Renderer *renderer) {
    this->drawColoredButton(renderer, 40);
}

void MainMenuButton::drawColoredButton(Renderer *renderer, Uint8 delta) {
    SDL_Color *newColor = renderer->getDeltaedColor(this->color, delta);

    renderer->drawColoredRect(newColor, this->x, this->y, this->width, this->height);

    SDL_Rect griffeDim = {this->x + 2, this->y + 2, (this->width - 4 - 2) * 20 / 100, this->height - 4};
    SDL_Rect textDim = {this->x + 2 + griffeDim.w + 2, this->y + 2, (this->width - 4 - 2) * 80 / 100, this->height - 4};

    renderer->drawTexture("texture/mainmenu/griffe.png", griffeDim.x, griffeDim.y, griffeDim.w, griffeDim.h);
    renderer->drawString(this->text, textDim.x, textDim.y, textDim.w, textDim.h);
}

