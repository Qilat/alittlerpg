//
// Created by Théophile Vieux on 2019-01-16.
//

#ifndef ALITTLERPG_MAINMENUSTAGE_H
#define ALITTLERPG_MAINMENUSTAGE_H

#include "../../../engine/stage/MenuStage.h"
#include "MainMenuUi.h"

class MainMenuStage : public MenuStage {

public:
    void init(Window *window) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void cleanup() override;
};


#endif //ALITTLERPG_MAINMENUSTAGE_H
