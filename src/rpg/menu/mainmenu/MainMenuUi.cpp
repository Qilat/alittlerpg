//
// Created by Théophile Vieux on 2019-01-22.
//

#include "MainMenuUi.h"

void MainMenuUi::init(Window *window) {
    this->backgroundImage = new Image(0, const_cast<char *>("texture/mainmenu/background.png"));

    this->bannerImage = new Image(1, const_cast<char *>("texture/mainmenu/banner.png"));
    this->bannerImage->setY(50);

    auto *color = new SDL_Color{254, 120, 40, 255};

    this->singleplayerButton = new MainMenuButton(0, const_cast<char *>("Un joueur"), color);
    this->multiplayerButton = new MainMenuButton(1, const_cast<char *>("Multijoueur"), color);
    this->settingsButton = new MainMenuButton(2, const_cast<char *>("Parametres"), color);
    this->quitButton = new MainMenuButton(3, const_cast<char *>("Quitter le jeu"), color);


    this->singleplayerButton->setOnPressListener([](Button *button) {
        StageHandler::startFirstLevel();
    });

    this->multiplayerButton->setOnPressListener([](Button *button) {

    });

    this->settingsButton->setOnPressListener([](Button *button) {

    });

    this->quitButton->setOnPressListener([](Button *button) {
        StageHandler::stopGame();
    });


    this->addElement(this->backgroundImage);
    this->addElement(this->bannerImage);
    this->addElement(this->singleplayerButton);
    this->addElement(this->multiplayerButton);
    this->addElement(this->settingsButton);
    this->addElement(this->quitButton);

    Ui::init(window);
}

const SDL_Rect minButtonDim = {0, 0, 80, 30};

void MainMenuUi::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    Ui::update(window, interval, inputs, renderer);

    this->backgroundImage->setX(0);
    this->backgroundImage->setY(0);
    this->backgroundImage->setWidth(window->getWidth());
    this->backgroundImage->setHeight(window->getHeight());

    this->bannerImage->setWidth(window->getWidth() * 45 / 100);
    this->bannerImage->setHeight(static_cast<int>(this->bannerImage->getWidth() / 2.07f));
    this->bannerImage->setX(window->getWidth() / 2 - this->bannerImage->getWidth() / 2);

    SDL_Rect calculatedDim = {0, 0, window->getWidth() * 25 / 100,
                              static_cast<int>(window->getWidth() * 20 / 100 / 3.0)};
    this->singleplayerButton->setWidth(calculatedDim.w >= minButtonDim.w ? calculatedDim.w : minButtonDim.w);
    this->singleplayerButton->setHeight(calculatedDim.h >= minButtonDim.h ? calculatedDim.h : minButtonDim.h);
    this->singleplayerButton->setX(window->getWidth() / 2 - this->singleplayerButton->getWidth() / 2);
    this->singleplayerButton->setY(
            this->bannerImage->getY() + this->bannerImage->getHeight() + (window->getHeight() * 5 / 100));


    this->multiplayerButton->setWidth(calculatedDim.w >= minButtonDim.w ? calculatedDim.w : minButtonDim.w);
    this->multiplayerButton->setHeight(calculatedDim.h >= minButtonDim.h ? calculatedDim.h : minButtonDim.h);
    this->multiplayerButton->setX(window->getWidth() / 2 - this->multiplayerButton->getWidth() / 2);
    this->multiplayerButton->setY(
            this->singleplayerButton->getY() + this->singleplayerButton->getHeight() + (window->getHeight() * 5 / 100));


    this->settingsButton->setWidth(calculatedDim.w >= minButtonDim.w ? calculatedDim.w : minButtonDim.w);
    this->settingsButton->setHeight(calculatedDim.h >= minButtonDim.h ? calculatedDim.h : minButtonDim.h);
    this->settingsButton->setX(window->getWidth() / 2 - this->settingsButton->getWidth() / 2);
    this->settingsButton->setY(
            this->multiplayerButton->getY() + this->multiplayerButton->getHeight() + (window->getHeight() * 5 / 100));


    this->quitButton->setWidth(calculatedDim.w >= minButtonDim.w ? calculatedDim.w : minButtonDim.w);
    this->quitButton->setHeight(calculatedDim.h >= minButtonDim.h ? calculatedDim.h : minButtonDim.h);
    this->quitButton->setX(window->getWidth() / 2 - this->quitButton->getWidth() / 2);
    this->quitButton->setY(
            this->settingsButton->getY() + this->settingsButton->getHeight() + (window->getHeight() * 5 / 100));
}

