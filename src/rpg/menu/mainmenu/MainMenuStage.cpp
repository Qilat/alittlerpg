//
// Created by Théophile Vieux on 2019-01-16.
//

#include "MainMenuStage.h"

void MainMenuStage::init(Window *window) {
    this->setUserInterface(new MainMenuUi());

    MenuStage::init(window);
}

void MainMenuStage::input(Window *window, Inputs *inputs) {
    MenuStage::input(window, inputs);
}

void MainMenuStage::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    MenuStage::update(window, interval, inputs, renderer);
}

void MainMenuStage::cleanup() {
    MenuStage::cleanup();
}
