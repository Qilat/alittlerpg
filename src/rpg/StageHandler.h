//
// Created by Théophile Vieux on 2019-01-26.
//

#ifndef ALITTLERPG_STAGEHANDLER_H
#define ALITTLERPG_STAGEHANDLER_H

#include "../engine/GameEngine.h"

class GameEngine;

class StageHandler {

public:

    explicit StageHandler(GameEngine *gameEngine);

    static void showMainMenu();

    static void startFirstLevel();

    static void stopGame();

private:
    static StageHandler *instance;

    void switchToStage(Stage *stage);

    GameEngine *gameEngine;
};


#endif //ALITTLERPG_STAGEHANDLER_H
