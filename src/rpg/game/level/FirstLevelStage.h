//
// Created by Théophile Vieux on 2019-01-26.
//

#ifndef ALITTLERPG_FIRSTLEVELSTAGE_H
#define ALITTLERPG_FIRSTLEVELSTAGE_H

#include "../../../engine/stage/Stage.h"
#include "../player/Player.h"
#include "../../../engine/element/game/Map.h"

class FirstLevelStage : public Stage {

public:

    void init(Window *window) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void draw(Window *window, Renderer *renderer) override;

    void cleanup() override;

private:
    Player *player = nullptr;
    Map *map = nullptr;
};


#endif //ALITTLERPG_FIRSTLEVELSTAGE_H
