//
// Created by Théophile Vieux on 2019-01-26.
//

#include "FirstLevelStage.h"

void FirstLevelStage::init(Window *window) {
    Stage::init(window);

    this->player = new Player(0);
    this->player->init(window);

    this->map = new Map(0, const_cast<char *>("map/home.tmx"));

}

void FirstLevelStage::input(Window *window, Inputs *inputs) {
    Stage::input(window, inputs);
    this->player->input(window, inputs);
    this->map->input(window, inputs);
}

void FirstLevelStage::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    Stage::update(window, interval, inputs, renderer);
    this->player->update(window, interval, inputs, renderer);
    this->map->update(window, interval, inputs, renderer);
}

void FirstLevelStage::draw(Window *window, Renderer *renderer) {
    Stage::draw(window, renderer);
    this->player->draw(window, renderer);
    this->map->draw(window, renderer);
}

void FirstLevelStage::cleanup() {
    this->player->cleanup();
    this->map->cleanup();
}
