//
// Created by Théophile Vieux on 2019-01-28.
//

#include "GameUI.h"

void GameUI::init(Window *window) {
    Ui::init(window);
}

void GameUI::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    Ui::update(window, interval, inputs, renderer);
}
