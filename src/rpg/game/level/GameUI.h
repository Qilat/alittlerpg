//
// Created by Théophile Vieux on 2019-01-28.
//

#ifndef ALITTLERPG_GAMEUI_H
#define ALITTLERPG_GAMEUI_H


#include "../../../engine/stage/Ui.h"

class GameUI : public Ui {
public:
    void init(Window *window) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;


private:

};


#endif //ALITTLERPG_GAMEUI_H
