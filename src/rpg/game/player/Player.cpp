
//
// Created by Théophile Vieux on 2019-01-16.
//

#include "Player.h"

#define DEPLACEMENT 1

Player::Player(int id) : Humanoid(id) {
}

void Player::init(Window *window) {
    Humanoid::init(window);

    this->setName(const_cast<char *>("Player"));
    this->setWidth(80);
    this->setHeight(60);

    //this->getBody()->setSex(FEMALE);
    this->getBody()->setRace(LIGHT_RACE);
    this->getBody()->setEars(ELVEN_EARS);
    this->getBody()->setEyes(RED_EYES);
    this->getBody()->setNose(STRAIGHT_NOSE);

    this->setX(window->getWidth() / 2);
    this->setY(window->getHeight() / 2);
}

void Player::draw(Window *window, Renderer *renderer) {
    Humanoid::draw(window, renderer);
}


void Player::input(Window *window, Inputs *inputs) {
    Humanoid::input(window, inputs);

    bool right = inputs->key[SDL_SCANCODE_RIGHT],
    left = inputs->key[SDL_SCANCODE_LEFT],
    up = inputs->key[SDL_SCANCODE_UP],
    down = inputs->key[SDL_SCANCODE_DOWN];

    bool moving = false;
    bool sprinting = inputs->key[SDL_SCANCODE_SPACE];

    if (left || right || down || up)
        moving = true;

    if (up && !down & !left & !right) {
        this->humanoidState->setFacing(FACING_TOP);
    } else if (!up && down && !left && !right) {
        this->humanoidState->setFacing(FACING_BOTTOM);
    } else if (!up && !down && left && !right) {
        this->humanoidState->setFacing(FACING_LEFT);
    } else if (!up && !down && !left && right) {
        this->humanoidState->setFacing(FACING_RIGHT);
    } else if (up && left && !down && !right) {
        this->humanoidState->setFacing(FACING_TOP);
    } else if (up && right && !down && !left) {
        this->humanoidState->setFacing(FACING_TOP);
    } else if (down && left && !up && !right) {
        this->humanoidState->setFacing(FACING_BOTTOM);
    } else if (down && right && !up && !left) {
        this->humanoidState->setFacing(FACING_BOTTOM);
    } else if (up && down && !left && !right) {
        moving = false;
        up = false;
        down = false;
    } else if (!up && !down && left && right) {
        moving = false;
        left = false;
        right = false;
    } else if (!up && !down & !left & !right) {
        moving = false;
    }

    if (moving) {

        if (this->isSpellCasting()) {
            this->stopSpellCasting();
        }

        if (sprinting && !this->isSprinting()) {
            this->sprint();
        } else if (!sprinting && this->isSprinting()) {
            this->stopSprinting();
        }

        if (!this->isWalking()) {
            this->walk();
        }

        int deplacement = DEPLACEMENT * (this->isSprinting() ? 2 : 1);

        if (left) {
            this->setX(this->getX() - deplacement);
        }
        if (right) {
            this->setX(this->getX() + deplacement);
        }
        if (down) {
            this->setY(this->getY() + deplacement);
        }
        if (up) {
            this->setY(this->getY() - deplacement);
        }
    } else {
        if (this->isSprinting()) {
            this->stopSprinting();
        } else if (this->isWalking()) {
            this->stopWalking();
        }

        bool spellcasting = inputs->key[SDL_SCANCODE_M];

        if (spellcasting) {
            this->spellCast();
        }
    }
}


void Player::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    Humanoid::update(window, interval, inputs, renderer);

}

void Player::cleanup() {
    Humanoid::cleanup();
}






