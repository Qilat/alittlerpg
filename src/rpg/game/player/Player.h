//
// Created by Théophile Vieux on 2019-01-16.
//

#ifndef ALITTLERPG_PLAYER_H
#define ALITTLERPG_PLAYER_H

#include "../humanoid/Humanoid.h"

class Player : public Humanoid {
public:
    explicit Player(int id);

    void init(Window *window) override;

    void cleanup() override;

    void draw(Window *window, Renderer *renderer) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void input(Window *window, Inputs *inputs) override;

};


#endif //ALITTLERPG_PLAYER_H
