//
// Created by Théophile Vieux on 2019-01-28.
//

#ifndef ALITTLERPG_RENDERHUMANOID_H
#define ALITTLERPG_RENDERHUMANOID_H

#include <vector>
#include "body/BodyRenderer.h"
#include "animation/SpellcastAnimation.h"
#include "animation/WalkingAnimation.h"

class HumanoidRenderer {
public:
    HumanoidRenderer(Body *body, HumanoidState *humanoidState);

    void update(float interval, Renderer *renderer);

    void draw(Renderer *renderer);

    void cleanup();

    void startWalkAnimation();
    void stopWalkAnimation();

    void startSpellCastAnimation();
    void stopSpellCastAnimation();

    void startThrustAnimation();
    void stopThrustAnimation();

    void startSlashAnimation();
    void stopSlashAnimation();

    void startShootAnimation();
    void stopShootAnimation();

    void startHurtAnimation();
    void stopHurtAnimation();

private:
    int frameX = 0;
    int frameY = 0;
    int textureWidth = 0;
    int textureHeight = 0;
    int frameWidth = 0;
    int frameHeight = 0;

    BodyRenderer *bodyRenderer = nullptr;
    HumanoidState *humanoidState;
    std::vector<Animation*> animations;
};


#endif //ALITTLERPG_RENDERHUMANOID_H
