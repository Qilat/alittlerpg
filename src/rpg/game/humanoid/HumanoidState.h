//
// Created by Théophile Vieux on 2019-02-27.
//

#ifndef ALITTLERPG_HUMANOIDSTATE_H
#define ALITTLERPG_HUMANOIDSTATE_H

#include "HumanoidDefinition.h"

class HumanoidState {

public:
    HumanoidState();

    int getX() const;

    void setX(int x);

    int getY() const;

    void setY(int y);

    int getWidth() const;

    void setWidth(int width);

    int getHeight() const;

    void setHeight(int height);

    int getAction();

    void setAction(int action);

    int getFacing();

    void setFacing(int facing);

    bool isSprinting() const;

    void setSprinting(bool sprinting);

private:
    int x, y, width, height;

    int action = STATIC;
    int facing = FACING_TOP;
    bool sprinting = false;

};


#endif //ALITTLERPG_HUMANOIDSTATE_H
