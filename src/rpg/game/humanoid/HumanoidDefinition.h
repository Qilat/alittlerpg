//
// Created by Théophile Vieux on 2019-01-27.
//

#ifndef ALITTLERPG_HUMANOIDDEFINITION_H
#define ALITTLERPG_HUMANOIDDEFINITION_H

#define RUNNING_SPEED 0.05F
#define WALKING_SPEED 0.15F

#define STATIC (-1)
#define SPELLCAST 0
#define THRUST 1
#define WALK 2
#define SLASH 3
#define SHOOT 4
#define HURT 5

#define SPELLCAST_FRAMES_AMOUNT 7
#define THRUST_FRAMES_AMOUNT 8
#define WALK_FRAMES_AMOUNT 9
#define SLASH_FRAMES_AMOUNT 6
#define SHOOT_FRAMES_AMOUNT 13
#define HURT_FRAMES_AMOUNT 6

#define FACING_TOP 0
#define FACING_LEFT 1
#define FACING_BOTTOM 2
#define FACING_RIGHT 3


#define ROOT_PATH "texture/humanoid/"

#define SEX_PATTERN "{sex}"

#define DEFAULT_SEX 0
#define MALE DEFAULT_SEX
#define MALE_PATH "male"
#define FEMALE 1
#define FEMALE_PATH "female"

#define RACE_PATTERN "{race}"

#define DEFAULT_RACE 0
#define DARK_RACE DEFAULT_RACE
#define DARK_RACE_NAME "dark"
#define DARK2_RACE 1
#define DARK2_RACE_NAME "dark2"
#define DARKELF_RACE 2
#define DARKELF_RACE_NAME "darkelf"
#define DARKELF2_RACE 3
#define DARKELF2_RACE_NAME "darkelf2"
#define LIGHT_RACE 4
#define LIGHT_RACE_NAME "light"
#define ORC_RACE 5
#define ORC_RACE_NAME "orc"
#define REDORC_RACE 6
#define REDORC_RACE_NAME "red_orc"
#define SKELETON_RACE 7
#define SKELETON_RACE_NAME "skeleton"
#define TANNED_RACE 8
#define TANNED_RACE_NAME "tanned"
#define TANNED2_RACE 9
#define TANNED2_RACE_NAME "tanned2"

#define SEXUAL_RACED_BODY_PATH ROOT_PATH "body/" SEX_PATTERN "/" RACE_PATTERN ".png"

#define EARS_ROOT_PATH ROOT_PATH "body/" SEX_PATTERN "/ears/"
#define DEFAULT_EARS 0
#define BIG_EARS 1
#define BIG_EARS_PATH EARS_ROOT_PATH "bigears_" RACE_PATTERN ".png"
#define ELVEN_EARS 2
#define ELVEN_EARS_PATH EARS_ROOT_PATH "elvenears_" RACE_PATTERN ".png"

#define EYES_ROOT_PATH ROOT_PATH "body/" SEX_PATTERN "/eyes/"
#define DEFAULT_EYES 0
#define BLUE_EYES 1
#define BLUE_EYES_PATH EYES_ROOT_PATH "blue.png"
#define BROWN_EYES 2
#define BROWN_EYES_PATH EYES_ROOT_PATH "brown.png"
#define GRAY_EYES 3
#define GRAY_EYES_PATH EYES_ROOT_PATH "gray.png"
#define GREEN_EYES 4
#define GREEN_EYES_PATH EYES_ROOT_PATH "green.png"
#define ORANGE_EYES 5
#define ORANGE_EYES_PATH EYES_ROOT_PATH "orange.png"
#define PURPLE_EYES 6
#define PURPLE_EYES_PATH EYES_ROOT_PATH "purple.png"
#define RED_EYES 7
#define RED_EYES_PATH EYES_ROOT_PATH "red.png"
#define YELLOW_EYES 8
#define YELLOW_EYES_PATH EYES_ROOT_PATH "yellow.png"
#define SKELETON_EYE 9
#define SKELETON_EYES_PATH EYES_ROOT_PATH "casting_eyeglow_skeleton.png"

#define NOSE_ROOT_PATH ROOT_PATH "body/" SEX_PATTERN "/nose/"
#define DEFAULT_NOSE 0
#define BIG_NOSE 1
#define BIG_NOSE_PATH NOSE_ROOT_PATH "bignose_" RACE_PATTERN ".png"
#define BUTTON_NOSE 2
#define BUTTON_NOSE_PATH NOSE_ROOT_PATH "buttonnose_" RACE_PATTERN ".png"
#define STRAIGHT_NOSE 3
#define STRAIGHT_NOSE_PATH NOSE_ROOT_PATH "straightnose_" RACE_PATTERN ".png"

#endif //ALITTLERPG_HUMANOIDDEFINITION_H
