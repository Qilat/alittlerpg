//
// Created by Théophile Vieux on 2019-01-27.
//

#include "Humanoid.h"

Humanoid::Humanoid(int id) : GameElement(id) {
}

//GENERAL
Body* Humanoid::getBody() {
    return body;
}

void Humanoid::setBody(Body *body) {
    Humanoid::body = body;
}

//WALKING PART
void Humanoid::walk() {
    this->humanoidRenderer->startWalkAnimation();
    this->humanoidState->setAction(WALK);
    this->humanoidState->setSprinting(false);
}
bool Humanoid::isWalking() {
    return this->humanoidState->getAction() == WALK;
}
void Humanoid::stopWalking() {
    this->humanoidRenderer->stopWalkAnimation();
    this->humanoidState->setAction(STATIC);
}

// SPRINTING PART
void Humanoid::sprint() {
    this->walk();
    this->humanoidState->setSprinting(true);
}
bool Humanoid::isSprinting() {
    return this->humanoidState->getAction() == WALK && this->humanoidState->isSprinting();
}
void Humanoid::stopSprinting() {
    this->stopWalking();
    this->humanoidState->setSprinting(false);
}

void Humanoid::spellCast() {
    this->humanoidRenderer->startSpellCastAnimation();
    this->humanoidState->setAction(SPELLCAST);
}
bool Humanoid::isSpellCasting() {
    return this->humanoidState->getAction() == SPELLCAST;
}
void Humanoid::stopSpellCasting() {
    this->humanoidRenderer->stopSpellCastAnimation();
    this->humanoidState->setAction(STATIC);
}

void Humanoid::thrust() {
    this->humanoidRenderer->startThrustAnimation();
    this->humanoidState->setAction(THRUST);
}
bool Humanoid::isThrusting() {
    return this->humanoidState->getAction() == THRUST;
}
void Humanoid::stopThrusting() {
    this->humanoidRenderer->stopThrustAnimation();
    this->humanoidState->setAction(STATIC);
}

void Humanoid::slash() {
    this->humanoidRenderer->startSlashAnimation();
    this->humanoidState->setAction(SLASH);
}
bool Humanoid::isSlashing() {
    return this->humanoidState->getAction() == SLASH;
}
void Humanoid::stopSlashing() {
    this->humanoidRenderer->stopSlashAnimation();
    this->humanoidState->setAction(STATIC);
}

void Humanoid::shoot() {
    this->humanoidRenderer->startShootAnimation();
    this->humanoidState->setAction(SHOOT);
}
bool Humanoid::isShooting() {
    return this->humanoidState->getAction() == SHOOT;
}
void Humanoid::stopShooting() {
    this->humanoidRenderer->stopShootAnimation();
    this->humanoidState->setAction(STATIC);
}

void Humanoid::hurt() {
    this->humanoidRenderer->startHurtAnimation();
    this->humanoidState->setAction(HURT);
}
bool Humanoid::isHurting() {
    return this->humanoidState->getAction() == HURT;
}
void Humanoid::stopHurting() {
    this->humanoidRenderer->stopHurtAnimation();
    this->humanoidState->setAction(STATIC);
}

void Humanoid::init(Window *window) {
    GameElement::init(window);
    this->body = new Body();
    this->humanoidState = new HumanoidState();
    this->humanoidRenderer = new HumanoidRenderer(this->body, this->humanoidState);
}

void Humanoid::input(Window *window, Inputs *inputs) {
    GameElement::input(window, inputs);
}

void Humanoid::update(Window *window, float interval, Inputs *inputs, Renderer *renderer) {
    GameElement::update(window, interval, inputs, renderer);

    this->humanoidState->setX(this->x);
    this->humanoidState->setY(this->y);
    this->humanoidState->setWidth(this->width);
    this->humanoidState->setHeight(this->height);

    this->humanoidRenderer->update(interval, renderer);
}

void Humanoid::draw(Window *window, Renderer *renderer) {
    GameElement::draw(window, renderer);
    this->humanoidRenderer->draw(renderer);
}

void Humanoid::cleanup() {
    GameElement::cleanup();
    this->humanoidRenderer->cleanup();
    free(this->humanoidRenderer);
    free(this->body);
}

void Humanoid::setX(int x) {
    Rendered::setX(x);
    this->humanoidState->setX(x);
}

void Humanoid::setY(int y) {
    Rendered::setY(y);
    this->humanoidState->setY(y);
}

void Humanoid::setWidth(int width) {
    Rendered::setWidth(width);
    this->humanoidState->setWidth(width);
}

void Humanoid::setHeight(int height) {
    Rendered::setHeight(height);
    this->humanoidState->setHeight(height);
}











