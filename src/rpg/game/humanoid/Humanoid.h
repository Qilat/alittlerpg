//
// Created by Théophile Vieux on 2019-01-27.
//

#ifndef ALITTLERPG_HUMANOID_H
#define ALITTLERPG_HUMANOID_H

#include "../../../engine/element/game/GameElement.h"
#include "HumanoidRenderer.h"

class Humanoid : public GameElement{

public:
    explicit Humanoid(int id);

    void init(Window *window) override;

    void input(Window *window, Inputs *inputs) override;

    void update(Window *window, float interval, Inputs *inputs, Renderer *renderer) override;

    void draw(Window *window, Renderer *renderer) override;

    void cleanup() override;

    void setX(int x) override;

    void setY(int y) override;

    void setWidth(int width) override;

    void setHeight(int height) override;


    void walk();
    bool isWalking();
    void stopWalking();

    void sprint();
    bool isSprinting();
    void stopSprinting();

    void spellCast();
    bool isSpellCasting();
    void stopSpellCasting();

    void thrust();
    bool isThrusting();
    void stopThrusting();

    void slash();
    bool isSlashing();
    void stopSlashing();

    void shoot();
    bool isShooting();
    void stopShooting();

    void hurt();
    bool isHurting();
    void stopHurting();

    Body *getBody();

protected:
    void setBody(Body *body);

    Body *body = nullptr;
    HumanoidRenderer *humanoidRenderer = nullptr;
    HumanoidState *humanoidState = nullptr;

};


#endif //ALITTLERPG_HUMANOID_H
