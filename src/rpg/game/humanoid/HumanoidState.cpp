//
// Created by Théophile Vieux on 2019-02-27.
//

#include "HumanoidState.h"

int HumanoidState::getX() const {
    return x;
}

void HumanoidState::setX(int x) {
    HumanoidState::x = x;
}

int HumanoidState::getY() const {
    return y;
}

void HumanoidState::setY(int y) {
    HumanoidState::y = y;
}

int HumanoidState::getWidth() const {
    return width;
}

void HumanoidState::setWidth(int width) {
    HumanoidState::width = width;
}

int HumanoidState::getHeight() const {
    return height;
}

void HumanoidState::setHeight(int height) {
    HumanoidState::height = height;
}

int HumanoidState::getAction() {
    return action;
}

void HumanoidState::setAction(int action) {
    this->action = action;
}

int HumanoidState::getFacing() {
    return this->facing;
}

void HumanoidState::setFacing(int facing) {
    this->facing = facing;
}

bool HumanoidState::isSprinting() const {
    return sprinting;
}

void HumanoidState::setSprinting(bool sprinting) {
    HumanoidState::sprinting = sprinting;
}

HumanoidState::HumanoidState() : x(0), y(0), width(0), height(0) {}
