//
// Created by Théophile Vieux on 2019-01-28.
//

#include "HumanoidRenderer.h"

HumanoidRenderer::HumanoidRenderer(Body *body, HumanoidState *humanoidState) : humanoidState(humanoidState),
                                                                               bodyRenderer(new BodyRenderer(body)) {
    animations = {new SpellcastAnimation(), nullptr, new WalkingAnimation(humanoidState), nullptr, nullptr};
}

void HumanoidRenderer::draw(Renderer *renderer) {
    renderer->drawTexture(this->bodyRenderer->getTexture(), this->frameX, this->frameY, this->frameWidth,
                          this->frameHeight,
                          this->humanoidState->getX(), this->humanoidState->getY(),
                          this->humanoidState->getWidth(), this->humanoidState->getHeight());
}

void HumanoidRenderer::update(float interval, Renderer *renderer) {
    this->bodyRenderer->updateTexture(renderer);

    renderer->getTextureDim(this->bodyRenderer->getTexture(), &this->textureWidth, &this->textureHeight);

    this->frameHeight = this->textureHeight / 21; // 21 = amount of row in a standard body png file
    this->frameWidth = this->textureWidth / 13; // 13 = max amount of columns in a standard body png file

    if (this->humanoidState->getAction() == STATIC) {
        this->frameX = 0 * this->frameWidth;
        this->frameY = (WALK * 4 + this->humanoidState->getFacing()) * frameHeight;;
    } else {
        animations[this->humanoidState->getAction()]->update(interval);
        animations[this->humanoidState->getAction()]->getFramePos(this->humanoidState->getAction(), this->frameHeight,
                                                                  this->humanoidState->getFacing(), this->frameX,
                                                                  this->frameY,
                                                             this->frameWidth);
    }
}

void HumanoidRenderer::cleanup() {
    this->bodyRenderer->cleanup();
}


void HumanoidRenderer::startWalkAnimation() {
    auto *walkingAnimation = dynamic_cast<WalkingAnimation *>(animations[WALK]);
    walkingAnimation->runCycle();
}

void HumanoidRenderer::stopWalkAnimation() {
    auto *walkingAnimation = dynamic_cast<WalkingAnimation *>(animations[WALK]);
    walkingAnimation->stop();
}

void HumanoidRenderer::startSpellCastAnimation() {
    auto *spellcastAnimation = dynamic_cast<SpellcastAnimation *>(animations[SPELLCAST]);
    spellcastAnimation->runOnce();
}

void HumanoidRenderer::stopSpellCastAnimation() {
    auto *spellcastAnimation = dynamic_cast<SpellcastAnimation *>(animations[SPELLCAST]);
    spellcastAnimation->stop();
}

void HumanoidRenderer::startThrustAnimation() {

}

void HumanoidRenderer::startSlashAnimation() {

}

void HumanoidRenderer::stopThrustAnimation() {

}

void HumanoidRenderer::stopSlashAnimation() {

}

void HumanoidRenderer::stopHurtAnimation() {

}

void HumanoidRenderer::startHurtAnimation() {

}

void HumanoidRenderer::stopShootAnimation() {

}

void HumanoidRenderer::startShootAnimation() {

}

