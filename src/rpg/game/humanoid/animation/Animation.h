//
// Created by Théophile Vieux on 2019-01-28.
//

#ifndef ALITTLERPG_ANIMATION_H
#define ALITTLERPG_ANIMATION_H

#include <cmath>
#include "../HumanoidDefinition.h" //One import for all children

class Animation {

public:
    bool isRunning() const;
    void setRunning(bool running);
    bool isRunningCycle() const;
    void setRunningCycle(bool runningCycle);

    int getFrameId() const;
    void runOnce();
    void runCycle();
    void update(float interval);
    void stop();

    virtual void setOnFinishCallback(void (*onFinishCallback)(Animation *));


    virtual int getFrameAmount();
    virtual float getFrameDuration();

    virtual void getFramePos(int action, int frameHeight, int facing, int &frameX, int &frameY, int frameWidth);

private:
    bool running = false;
    bool runningCycle = false;
    int frameId = 0;
    float intervalSum = 0;

    void (*onFinishCallback)(Animation *) = nullptr;

};


#endif //ALITTLERPG_ANIMATION_H
