//
// Created by Théophile Vieux on 2019-01-28.
//

#include "Animation.h"

bool Animation::isRunning() const {
    return running;
}

void Animation::setRunning(bool running) {
    Animation::running = running;
}

void Animation::runOnce() {
    this->running = true;
}

void Animation::runCycle() {
    this->running = true;
    this->runningCycle = true;
}

void Animation::update(float interval) {
    if(this->isRunning()) {
        this->intervalSum += interval;
        this->frameId = (int) round(this->intervalSum / this->getFrameDuration()) % this->getFrameAmount();

        if(!this->isRunningCycle()){
            if(this->frameId == this->getFrameAmount())
                this->stop();
        }
    }
}

void Animation::stop() {
    this->running = false;
    this->frameId = 0;
    this->intervalSum = 0;

    if (this->onFinishCallback != nullptr)
        this->onFinishCallback(this);
}

int Animation::getFrameId() const {
    return frameId;
}

bool Animation::isRunningCycle() const {
    return runningCycle;
}

void Animation::setRunningCycle(bool runningCycle) {
    Animation::runningCycle = runningCycle;
}

int Animation::getFrameAmount() {
    return 0;
}

float Animation::getFrameDuration() {
    return 0;
}

void Animation::setOnFinishCallback(void (*onFinishCallback)(Animation *)) {
    this->onFinishCallback = onFinishCallback;
}

void Animation::getFramePos(int action, int frameHeight, int facing, int &frameX, int &frameY, int frameWidth) {
    frameX = this->getFrameId() * frameWidth;
    frameY = (action * 4 + facing) * frameHeight;
}
