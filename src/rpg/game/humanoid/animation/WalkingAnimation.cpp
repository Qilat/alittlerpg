//
// Created by Théophile Vieux on 2019-01-28.
//

#include "WalkingAnimation.h"

WalkingAnimation::WalkingAnimation(HumanoidState *humanoidState) : humanoidState(humanoidState) {}

int WalkingAnimation::getFrameAmount() {
    return WALK_FRAMES_AMOUNT;
}

float WalkingAnimation::getFrameDuration() {
    return this->humanoidState->isSprinting() ? RUNNING_SPEED : WALKING_SPEED;
}
