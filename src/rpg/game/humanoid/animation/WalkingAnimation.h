//
// Created by Théophile Vieux on 2019-01-28.
//

#ifndef ALITTLERPG_WALKINGANIMATION_H
#define ALITTLERPG_WALKINGANIMATION_H


#include "Animation.h"

#include "../HumanoidState.h"

class WalkingAnimation : public Animation {

public:
    explicit WalkingAnimation(HumanoidState *humanoidState);

    int getFrameAmount() override;

    float getFrameDuration() override;

private:
    HumanoidState *humanoidState;
};


#endif //ALITTLERPG_WALKINGANIMATION_H
