//
// Created by Théophile Vieux on 2019-01-29.
//

#ifndef ALITTLERPG_SPELLCASTANIMATION_H
#define ALITTLERPG_SPELLCASTANIMATION_H


#include "Animation.h"

class SpellcastAnimation : public Animation {


public:
    int getFrameAmount() override;

    float getFrameDuration() override;

};


#endif //ALITTLERPG_SPELLCASTANIMATION_H
