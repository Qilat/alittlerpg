//
// Created by Théophile Vieux on 2019-01-29.
//

#include "SpellcastAnimation.h"

int SpellcastAnimation::getFrameAmount() {
    return SPELLCAST_FRAMES_AMOUNT;
}

float SpellcastAnimation::getFrameDuration() {
    return 0.25F;
}
