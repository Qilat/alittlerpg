//
// Created by Théophile Vieux on 2019-01-28.
//

#ifndef ALITTLERPG_BODYRENDERER_H
#define ALITTLERPG_BODYRENDERER_H


#include "Body.h"
#include "../../../../engine/graphic/Renderer.h"

using namespace std;

class BodyRenderer {

public:
    explicit BodyRenderer(Body *body);

    void updateTexture(Renderer *renderer);

    SDL_Texture* getTexture();

    Body *getBody();

    void cleanup();

private:
    Body *body = nullptr;
    SDL_Texture *baseTexture = nullptr;
    SDL_Texture *earsTexture = nullptr;
    SDL_Texture *eyesTexture = nullptr;
    SDL_Texture *noseTexture = nullptr;

    void loadBaseTexture(Renderer *renderer);

    void loadEarsTexture(Renderer *renderer);

    void loadEyesTexture(Renderer *renderer);

    void loadNoseTexture(Renderer *renderer);

    string formatPath(string path);

};


#endif //ALITTLERPG_BODYRENDERER_H
