//
// Created by Théophile Vieux on 2019-01-27.
//

#ifndef ALITTLERPG_BODY_H
#define ALITTLERPG_BODY_H

#include "../HumanoidDefinition.h"

class Body {

public:
    Body();

    Body(int sex, int race, int ears, int eyes, int nose);

    int getSex();

    void setSex(int sex);

    int getRace();

    void setRace(int race);

    int getEars();

    void setEars(int ears);

    int getEyes();

    void setEyes(int eyes);

    int getNose();

    void setNose(int nose);

    bool isSexChange();

    void setSexChange(bool sexChange);

    bool isRaceChange();

    void setRaceChange(bool raceChange);

    bool isEarsChange();

    void setEarsChange(bool earsChange);

    bool isEyesChange();

    void setEyesChange(bool eyesChange);

    bool isNoseChange();

    void setNoseChange(bool noseChange);

private:
    int sex;
    int race;
    int ears;
    int eyes;
    int nose;
    bool sexChange = true;
    bool raceChange = true;
    bool earsChange = true;
    bool eyesChange = true;
    bool noseChange = true;

    void updateEarsEyesNose();
};


#endif //ALITTLERPG_BODY_H
