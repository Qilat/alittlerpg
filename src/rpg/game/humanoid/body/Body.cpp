//
// Created by Théophile Vieux on 2019-01-27.
//

#include "Body.h"

Body::Body() : Body(DEFAULT_SEX, DEFAULT_RACE, DEFAULT_EARS, DEFAULT_EYES, DEFAULT_NOSE) {}

Body::Body(int sex, int race, int ears, int eyes, int nose) {
    this->setSex(sex);
    this->setRace(race);
    this->setEars(ears);
    this->setEyes(eyes);
    this->setNose(nose);
}

int Body::getSex() {
    return sex;
}

void Body::setSex(int sex) {
    if (this->sex != sex) {
        this->sexChange = true;

        if (sex != MALE
            && sex != FEMALE) {
            Body::sex = DEFAULT_SEX;
            this->updateEarsEyesNose();
            return;
        }

        Body::sex = sex;
        this->updateEarsEyesNose();
    }
}

int Body::getRace() {
    return race;
}

void Body::setRace(int race) {
    if (this->race != race) {
        this->raceChange = true;

        if (race != DARK_RACE
            && race != DARK2_RACE
            && race != DARKELF_RACE
            && race != DARKELF2_RACE
            && race != LIGHT_RACE
            && race != ORC_RACE
            && race != REDORC_RACE
            && race != SKELETON_RACE
            && race != TANNED_RACE
            && race != TANNED2_RACE) {
            Body::race = DEFAULT_RACE;
            this->updateEarsEyesNose();
            return;
        }

        Body::race = race;
        this->updateEarsEyesNose();
    }
}

int Body::getEars() {
    return ears;
}

void Body::setEars(int ears) {
    if (this->ears != ears || this->sexChange || this->raceChange) {
        this->earsChange = true;
        //most frequent case
        if (ears == DEFAULT_EARS) {
            Body::ears = ears;
            return;
        }

        //cancel wrong ears
        if (ears != BIG_EARS
            && ears != ELVEN_EARS) {
            Body::ears = DEFAULT_EARS;
            return;
        }

        //cancel wrong race
        if (this->race == DARK_RACE
            || this->race == DARK2_RACE
            || this->race == DARKELF_RACE
            || this->race == DARKELF2_RACE
            || this->race == LIGHT_RACE
            || this->race == TANNED_RACE
            || this->race == TANNED2_RACE) {
            Body::ears = ears;
            return;
        }
        Body::ears = DEFAULT_EARS;
    }
}

int Body::getEyes() {
    return eyes;
}

void Body::setEyes(int eyes) {
    if (this->eyes != eyes || this->sexChange || this->raceChange) {
        this->earsChange = true;
        //most frequent case
        if (eyes == DEFAULT_EYES) {
            Body::eyes = eyes;
            return;
        }

        //cancel wrong ears
        if (eyes != BLUE_EYES
            && eyes != BROWN_EYES
            && eyes != GRAY_EYES
            && eyes != GREEN_EYES
            && eyes != ORANGE_EYES
            && eyes != PURPLE_EYES
            && eyes != RED_EYES
            && eyes != YELLOW_EYES
            && eyes != SKELETON_EYE) {
            Body::eyes = DEFAULT_EARS;
            return;
        }

        Body::eyes = eyes;
    }
}

int Body::getNose() {
    return nose;
}

void Body::setNose(int nose) {
    if (nose != this->nose || this->sexChange || this->raceChange) {
        this->noseChange = true;
        //most frequent case
        if (nose == DEFAULT_NOSE) {
            Body::nose = nose;
            return;
        }

        //cancel wrong ears
        if (nose != BIG_NOSE
            && nose != BUTTON_NOSE
            && nose != STRAIGHT_NOSE) {
            Body::nose = DEFAULT_NOSE;
            return;
        }

        //cancel wrong race
        if (this->race == DARK_RACE
            || this->race == DARK2_RACE
            || this->race == DARKELF_RACE
            || this->race == DARKELF2_RACE
            || this->race == LIGHT_RACE
            || this->race == TANNED_RACE
            || this->race == TANNED2_RACE) {
            Body::nose = nose;
            return;
        }
        Body::nose = DEFAULT_EARS;
    }
}

void Body::updateEarsEyesNose() {
    this->setEars(this->getEars());
    this->setEyes(this->getEyes());
    this->setNose(this->getNose());
}

bool Body::isSexChange() {
    return sexChange;
}

void Body::setSexChange(bool sexChange) {
    Body::sexChange = sexChange;
}

bool Body::isRaceChange() {
    return raceChange;
}

void Body::setRaceChange(bool raceChange) {
    Body::raceChange = raceChange;
}

bool Body::isEarsChange() {
    return earsChange;
}

void Body::setEarsChange(bool earsChange) {
    Body::earsChange = earsChange;
}

bool Body::isEyesChange() {
    return eyesChange;
}

void Body::setEyesChange(bool eyesChange) {
    Body::eyesChange = eyesChange;
}

bool Body::isNoseChange() {
    return noseChange;
}

void Body::setNoseChange(bool noseChange) {
    Body::noseChange = noseChange;
}