//
// Created by Théophile Vieux on 2019-01-28.
//

#include "BodyRenderer.h"

BodyRenderer::BodyRenderer(Body *body) : body(body) {}

Body *BodyRenderer::getBody() {
    return body;
}

string BodyRenderer::formatPath(string path) {

    size_t sexOccurence = path.find(SEX_PATTERN);
    if(sexOccurence != string::npos){
        string sex;
        switch (this->body->getSex()){
            case MALE:
                sex = MALE_PATH;
                break;
            case FEMALE:
                sex = FEMALE_PATH;
                break;
                default:
                    sex = MALE_PATH;
        }
        path.replace(sexOccurence, ((string)SEX_PATTERN).length(), sex);
    }

    size_t raceOccurence = path.find(RACE_PATTERN);
    if(raceOccurence != string::npos){
        string race;
        switch (this->body->getRace()){
            case DARK_RACE:
                race = DARK_RACE_NAME;
                break;
            case DARK2_RACE:
                race = DARK2_RACE_NAME;
                break;
            case DARKELF_RACE:
                race = DARKELF_RACE_NAME;
                break;
            case DARKELF2_RACE:
                race = DARKELF2_RACE_NAME;
                break;
            case LIGHT_RACE:
                race = LIGHT_RACE_NAME;
                break;
            case ORC_RACE:
                race = ORC_RACE_NAME;
                break;
            case REDORC_RACE:
                race = REDORC_RACE_NAME;
                break;
            case SKELETON_RACE:
                race = SKELETON_RACE_NAME;
                break;
            case TANNED_RACE:
                race = TANNED_RACE_NAME;
                break;
            case TANNED2_RACE:
                race = TANNED2_RACE_NAME;
                break;
            default:
                race = DARK_RACE_NAME;
                break;
        }
        path.replace(raceOccurence, ((string)RACE_PATTERN).length(), race);
    }
    return path;
}

void BodyRenderer::loadBaseTexture(Renderer *renderer) {
    this->body->setRaceChange(false);
    this->body->setSexChange(false);

    if (this->baseTexture != nullptr)
        Renderer::unloadTexture(this->baseTexture);
    string path = this->formatPath(SEXUAL_RACED_BODY_PATH);
    this->baseTexture = renderer->loadTexture(path.c_str());
}

void BodyRenderer::loadEarsTexture(Renderer *renderer) {
    this->body->setEarsChange(false);

    if (this->earsTexture != nullptr)
        Renderer::unloadTexture(this->earsTexture);
    string path;

    switch(this->getBody()->getEars()) {
        case DEFAULT_EARS:
            this->earsTexture = nullptr;
            return;
        case BIG_EARS:
            path = BIG_EARS_PATH;
            break;
        case ELVEN_EARS:
            path = ELVEN_EARS_PATH;
            break;
        default:
            this->earsTexture = nullptr;
            return;
    }

    path = this->formatPath(path);
    this->earsTexture = renderer->loadTexture(path.c_str());
}

void BodyRenderer::loadEyesTexture(Renderer *renderer) {
    this->body->setEyesChange(false);

    if (this->eyesTexture != nullptr)
        Renderer::unloadTexture(this->eyesTexture);
    string path;

    switch(this->getBody()->getEyes()) {
        case DEFAULT_EYES:
            this->eyesTexture = nullptr;
            return;
        case BLUE_EYES:
            path = BLUE_EYES_PATH;
            break;
        case BROWN_EYES:
            path = BROWN_EYES_PATH;
            break;
        case GRAY_EYES:
            path = GRAY_EYES_PATH;
            break;
        case GREEN_EYES:
            path = GREEN_EYES_PATH;
            break;
        case ORANGE_EYES:
            path = ORANGE_EYES_PATH;
            break;
        case PURPLE_EYES:
            path = PURPLE_EYES_PATH;
            break;
        case RED_EYES:
            path = RED_EYES_PATH;
            break;
        case YELLOW_EYES:
            path = YELLOW_EYES_PATH;
            break;
        case SKELETON_EYE:
            path = SKELETON_EYES_PATH;
            break;
        default:
            this->eyesTexture = nullptr;
            return;
    }

    path = this->formatPath(path);
    this->eyesTexture = renderer->loadTexture(path.c_str());
}

void BodyRenderer::loadNoseTexture(Renderer *renderer) {
    this->body->setNoseChange(false);

    if (this->noseTexture != nullptr)
        Renderer::unloadTexture(this->noseTexture);
    string path;

    switch(this->getBody()->getEars()) {
        case DEFAULT_EARS:
            this->noseTexture = nullptr;
            return;
        case BIG_NOSE:
            path = BIG_NOSE_PATH;
            break;
        case BUTTON_NOSE:
            path = BUTTON_NOSE_PATH;
            break;
        case STRAIGHT_NOSE:
            path = STRAIGHT_NOSE_PATH;
            break;
        default:
            this->noseTexture = nullptr;
            return;
    }

    path = this->formatPath(path);
    this->noseTexture = renderer->loadTexture(path.c_str());
}

void BodyRenderer::updateTexture(Renderer *renderer) {
    if(this->getBody()->isRaceChange() || this->getBody()->isSexChange() || this->getBody()->isEarsChange() || this->getBody()->isEyesChange() || this->getBody()->isNoseChange()) {
        this->loadBaseTexture(renderer);
        if (this->getBody()->isEarsChange())
            this->loadEarsTexture(renderer);
        if (this->getBody()->isEyesChange())
            this->loadEyesTexture(renderer);
        if (this->getBody()->isNoseChange())
            this->loadNoseTexture(renderer);

        renderer->setTextureRenderTarget(this->baseTexture);
        int width = 0, height = 0;
        renderer->getTextureDim(this->baseTexture, &width, &height);
        if (this->earsTexture != nullptr)
            renderer->drawTexture(this->earsTexture, 0, 0, width, height);
        if (this->eyesTexture != nullptr)
            renderer->drawTexture(this->eyesTexture, 0, 0, width, height);
        if (this->noseTexture != nullptr)
            renderer->drawTexture(this->noseTexture, 0, 0, width, height);
        renderer->resetTextureRenderTarget();
    }
}

SDL_Texture* BodyRenderer::getTexture() {
    return this->baseTexture;
}

void BodyRenderer::cleanup() {
    if (this->baseTexture != nullptr)
        Renderer::unloadTexture(this->baseTexture);
    if (this->earsTexture != nullptr)
        Renderer::unloadTexture(this->earsTexture);
    if (this->eyesTexture != nullptr)
        Renderer::unloadTexture(this->eyesTexture);
    if (this->noseTexture != nullptr)
        Renderer::unloadTexture(this->noseTexture);
}
