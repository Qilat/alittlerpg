//
// Created by Théophile Vieux on 2019-01-26.
//

#include "StageHandler.h"
#include "menu/mainmenu/MainMenuStage.h"
#include "game/level/FirstLevelStage.h"

StageHandler *StageHandler::instance;


void StageHandler::showMainMenu() {
    instance->switchToStage(new MainMenuStage());
}

void StageHandler::switchToStage(Stage *stage) {
    this->gameEngine->setCurrentStage(stage);
}

void StageHandler::stopGame() {
    instance->gameEngine->setFinish(true);
}

void StageHandler::startFirstLevel() {
    instance->switchToStage(new FirstLevelStage());
}

StageHandler::StageHandler(GameEngine *gameEngine) : gameEngine(gameEngine) {
    StageHandler::instance = this;
}
